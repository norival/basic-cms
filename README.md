# Basic CMS

Un petit CMS basique réalisé avec Symfony. Le but est d'avoir les
fonctionnalités CMS de base et de les étendre pour répondre aux besoins
spécifiques.

## Fonctionnalités de base :

- Gestion des pages : création, édition, suppression
- Gestion de l'arborescence du site
- Gestion de médias (images/documents)
- Configuration du site : logo, description, mentions légales
- Formulaire de contact avec protection Captcha

## Fonctionnalités spécifiques développées :

- Outil de création de formulaires
