<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CaptchaValidator
{
    private const VERIFICATION_URL = 'https://api.friendlycaptcha.com/api/v1/siteverify';

    public function __construct(
        private HttpClientInterface $httpClient,
        private ParameterBagInterface $params
    ) { }

    public function validate(string $solution): bool
    {
        $response = $this->httpClient->request(
            'POST',
            self::VERIFICATION_URL,
            [
                'json' => [
                    'solution' => $solution,
                    'secret'   => $this->params->get('app.friendlycaptcha.key'),
                ],
            ]
        );
        $content = \json_decode($response->getContent(), true);

        if ($response->getStatusCode() === 200 && $content['success'] === false) {
            return false;
        }

        return true;
    }
}
