<?php

namespace App\Service;

use App\Entity\Asset;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    const ASSET_PATH = '/uploads/assets/';

    public function __construct(
        private EntityManagerInterface $em,
        private string $targetDirectory,
        private SluggerInterface $slugger,
        private ParameterBagInterface $params
    ) { }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    /**
     * Remove a file from the file system
     *
     * @throws IOException
     */
    public function remove(string $path)
    {
        $filesystem = new Filesystem();
        $result = $filesystem->remove($this->getTargetDirectory() . $path);
    }

    /**
     * @throws FileException
     */
    public function upload(
        UploadedFile $file,
        string|null $fileName = null
    ): Array {
        // check if extension is allowed and define type based on extension
        $ext = $file->guessExtension();
        if (in_array($ext, $this->params->get('upload')['document']['ext'])) {
            $type = Asset::ALLOWED_TYPES['document'];
        } elseif (in_array($ext, $this->params->get('upload')['image']['ext'])) {
            $type = Asset::ALLOWED_TYPES['image'];
        } else {
            throw new FileException('bad extension');
        }

        if ($fileName === null) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
        }

        $file->move($this->getTargetDirectory(), $fileName);

        return [
            'filename' => $fileName,
            'type' => $type,
        ];
    }

    /**
     * @throws FileException
     * @throws IOException
     */
    public function replace(Asset $asset, UploadedFile $file): Array
    {
        $uploadedFile = $this->upload($file, $asset->getPath());

        $fileSystem = new Filesystem();

        // remove all cropped assets
        /** @var \App\Entity\CroppedAsset[] $croppedAssets */
        $croppedAssets = $asset->getCroppedAssets();
        foreach ($croppedAssets as $croppedAsset) {
            $fileSystem->remove($this->getTargetDirectory() . $croppedAsset->getPath());
            $this->em->remove($croppedAsset);
        }

        /** @var \App\Entity\PageAsset[] $pageAssets */
        $pageAssets = $asset->getPageAssets();
        foreach ($pageAssets as $pageAsset) {
            // replace content in pages where the image is present
            $page = $pageAsset->getPage();
            $content = $page->getContent();
            $content['blocks'] = array_map(
                function ($block) use ($asset) {
                    if ($block['type'] !== 'image' || $block['data']['id'] !== $asset->getId()) {
                        return $block;
                    }

                    // replace filename and remove crop data
                    $block['data']['crop'] = [];

                    return $block;
                },
                $content['blocks']
            );

            $page->setContent($content);
            $this->em->persist($page);
        }

        $this->em->flush();

        return $uploadedFile;
    }
}
