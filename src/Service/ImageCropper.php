<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImageCropper
{
    /**
     * crop
     *
     * @throws FileException
     */
    public function crop(string $filename, Array $rect): string
    {
        $imagegick = new \Imagick(realpath($filename));
        $imagegick->cropImage($rect['width'], $rect['height'], $rect['x'], $rect['y']);

        // change filename
        $infos = pathinfo($filename);
        $path = $infos['dirname']
            . '/'
            . $infos['filename']
            . '-cropped-' . uniqid() . '.'
            . $infos['extension'];

        // save image
        $imagegick->writeImage($path);

        // return new file name
        return pathinfo($path, PATHINFO_BASENAME);
    }
}
