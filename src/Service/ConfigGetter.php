<?php

namespace App\Service;

use App\Repository\ConfigRepository;

class ConfigGetter
{
    public function __construct(
        private ConfigRepository $configRepository
    ) { }

    public function get()
    {
        return $this->configRepository->findOneBy([]);
    }
}
