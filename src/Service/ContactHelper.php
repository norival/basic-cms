<?php

namespace App\Service;

use App\Entity\Contact;
use App\Repository\ConfigRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactHelper
{
    public function __construct(
        private CaptchaValidator $captchaValidator,
        private ConfigRepository $configRepository,
        private EntityManagerInterface $em,
        private MailerInterface $mailer,
        private TranslatorInterface $translator
    ) { }

    public function handleContact(
        string $captchaSolution,
        Contact $contact
    ): array {
        // captcha verification
        if ($this->captchaValidator->validate($captchaSolution)) {
            /** @var \App\Entity\Config $config */
            $config = $this->configRepository->findOneBy([]);

            // create the email
            $email = new TemplatedEmail();
            $email->from(new Address($contact->getEmail(), $contact->getName()))
                  ->to(new Address($config->getContactEmail(), $config->getContactName()))
                  ->subject(
                      $contact->getName() . ' ' . $this->translator->trans('vous a envoyé un message !')
                  )
                  ->htmlTemplate('mail/contact-fr.html.twig')
                  ->context([
                      'contact' => $contact,
                  ])
                  ;

            // send the email
            try {
                $this->mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                return [
                    'isSuccess' => false,
                    'message' => $this->translator->trans("Le message n'a pas pu être envoyé. Merci de ré-essayer"),
                ];
            }

            $this->em->persist($contact);
            $this->em->flush();

            return [
                'isSuccess' => true,
                'message' => $this->translator->trans('Votre message a bien été envoyé !'),
            ];
        }

        return [
            'isSuccess' => false,
            'message' => $this->translator->trans('La vérification CAPTCHA a échoué, merci de réessayer'),
        ];
    }

}
