<?php

namespace App\DataFixtures;

use App\Entity\Config;
use App\Entity\Menu;
use App\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProdFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');

        $mainMenu = new Menu();
        $mainMenu->setName('main_menu');
        $manager->persist($mainMenu);

        $root = new Page();
        $root->setIsEmptyNode(true);
        $root->setTitle('_root');
        $root->setSlug('_root');
        $root->setIsInHomepage(false);
        $root->setIsDraft(false);
        $root->setIsDeleted(false);
        $root->setIsHomepage(false);
        $manager->persist($root);

        $page = new Page();
        $page->setIsEmptyNode(false);
        $page->setUser($this->getReference(UserFixtures::ADMIN_USER));
        $page->setTitle('Homepage');
        $page->setSlug('home');
        $page->setIsInHomepage(false);
        $page->setIsDraft(false);
        $page->setIsDeleted(false);
        $page->setIsHomepage(true);
        $manager->persist($page);

        $config = new Config();
        $config->setSiteTitle($faker->sentence());
        $config->setSiteDescription($faker->paragraph());
        $config->setContactName('Norival Development');
        $config->setContactEmail('xavier@norival.dev');
        $config->setContactAddress(
            "2 rue de Jupiter\r\n54000 Nancy"
        );
        $config->setContactPhone('0606121212');
        $config->setLegalNotice($faker->paragraph());

        $manager->persist($config);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['prod'];
    }
}
