<?php

namespace App\DataFixtures;

use App\Entity\Block;
use App\Entity\Config;
use App\Entity\Menu;
use App\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture implements DependentFixtureInterface
{
    const VISIBLE_PAGES = [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
    ];
    const DRAFT_PAGES = [
        12, 13,
    ];
    const DELETED_PAGES = [
        14, 15, 16, 17, 18, 19, 20,
    ];

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');
        $mainMenu = new Menu();
        $mainMenu->setName('main_menu');
        $manager->persist($mainMenu);

        $footerMenu = new Menu();
        $footerMenu->setName('footer_menu');
        $manager->persist($footerMenu);

        $root = new Page();
        $root->setIsEmptyNode(true);
        $root->setTitle('_root');
        $root->setSlug('_root');
        $root->setIsInHomepage(false);
        $root->setIsDraft(false);
        $root->setIsDeleted(false);
        $root->setIsHomepage(false);
        $manager->persist($root);

        for ($p = 0; $p <= 20; $p++) {
            $page = new Page();
            $page->setUser($this->getReference(UserFixtures::ADMIN_USER));
            $page->setTitle($faker->sentence());
            $page->setSlug($faker->slug());
            $page->setDescription($faker->paragraph());
            $page->setIsDraft(false);
            $page->setIsDeleted(false);
            $page->setIsHomepage(false);
            $page->setIsInHomepage(true);
            $page->setHomepageOrder($p + 1);
            $page->setParent($root);
            $page->setContent([
                'time' => 1625096908029,
                'version' => '2.22.0',
                'blocks' => [
                    [
                        'id' => 'utX02rTdcE',
                        'type' => 'header',
                        'data' => [
                            'level' => 2,
                            'text' => $faker->sentence(),
                        ],
                    ],
                    [
                        'id' => 'utX02rTdcE',
                        'type' => 'paragraph',
                        'data' => [
                            'text' => $faker->paragraph(),
                        ],
                    ],
                    [
                        'id' => 'utX02rTdcE',
                        'type' => 'paragraph',
                        'data' => [
                            'text' => $faker->paragraph(),
                        ],
                    ],
                ],
            ]);
            $page->setIsEmptyNode(false);

            if ($p === 0) {
                $page->setIsHomepage(true);
                $page->setTitle('This is the homepage of the website');
            }

            if ($p <= 9) {
                $page->addMenu($mainMenu);
            }

            if (in_array($p, self::DRAFT_PAGES)) {
                $page->setIsDraft(true);
            }

            if (in_array($p, self::DELETED_PAGES)) {
                $page->setIsDeleted(true);
            }

            if ($p === 3 || $p === 4) {
                $page->setParent($this->getReference("page_2"));
            }

            if ($p === 9 || $p === 10) {
                $page->setParent($this->getReference("page_5"));
            }

            if ($p === 11 || $p === 12 || $p === 13) {
                $page->setParent($this->getReference("page_10"));
            }

            if ($p === 18 || $p === 19 || $p === 20) {
                $page->setParent($this->getReference("page_12"));
            }

            $this->addReference("page_$p", $page);
            $manager->persist($page);
        }

        $config = new Config();
        $config->setSiteTitle($faker->sentence());
        $config->setSiteDescription($faker->paragraph());
        $config->setContactName('Norival Development');
        $config->setContactEmail('xavier@norival.dev');
        $config->setContactAddress(
            "2 rue de Jupiter\r\n54000 Nancy"
        );
        $config->setContactPhone('0606121212');
        $config->setLegalNotice($faker->paragraph());

        $manager->persist($config);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
