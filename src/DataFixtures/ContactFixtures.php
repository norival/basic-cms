<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ContactFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i <= 20; $i++) {
            $message = new Contact();
            $message->setName($faker->userName);
            $message->setEmail($faker->email);
            $message->setMessage($faker->paragraph());
            $message->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime));

            $manager->persist($message);
        }

        $manager->flush();
    }
}
