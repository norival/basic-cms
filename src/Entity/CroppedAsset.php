<?php

namespace App\Entity;

use App\Repository\CroppedAssetRepository;
use App\Service\FileUploader;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CroppedAssetRepository::class)
 */
class CroppedAsset
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("media_collection")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("media_collection")
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity=Asset::class, inversedBy="croppedAssets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $asset;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getAsset(): ?Asset
    {
        return $this->asset;
    }

    public function setAsset(?Asset $asset): self
    {
        $this->asset = $asset;

        return $this;
    }

    /**
     * @Groups("media_collection")
     */
    public function getPublicPath(): string
    {
        return FileUploader::ASSET_PATH . $this->getPath();
    }
}
