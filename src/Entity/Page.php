<?php

namespace App\Entity;

use App\Repository\PageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("slug")
 */
class Page
{
    const TITLE_LENGTH = 40;
    const DESCRIPTION_LENGTH = 70;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\Regex('/^[a-z0-9]+(?:-[a-z0-9]+)*$/')]
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="pages")
     */
    private $user;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(name="tree_root", referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\ManyToMany(targetEntity=Menu::class, mappedBy="pages")
     */
    private $menus;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDraft;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isHomepage;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $content = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInHomepage;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $homepageOrder;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sortingOrder;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEmptyNode;

    /**
     * @ORM\OneToMany(targetEntity=PageAsset::class, mappedBy="page", orphanRemoval=true)
     */
    private $pageAssets;

    /**
     * @ORM\OneToOne(targetEntity=CroppedAsset::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $header;

    public function __construct()
    {
        $this->menus = new ArrayCollection();
        $this->pageAssets = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setParent(Page $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenus(): Collection
    {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self
    {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->addPage($this);
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self
    {
        if ($this->menus->removeElement($menu)) {
            $menu->removePage($this);
        }

        return $this;
    }

    public function getIsDraft(): ?bool
    {
        return $this->isDraft;
    }

    public function setIsDraft(bool $isDraft): self
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTime();
        $this->isDeleted = $this->isDeleted ?? false;
        $this->isHomepage = $this->isHomepage ?? false;
        $this->isEmptyNode = $this->isEmptyNode ?? false;
    }

    public function getIsHomepage(): ?bool
    {
        return $this->isHomepage;
    }

    public function setIsHomepage(bool $isHomepage): self
    {
        $this->isHomepage = $isHomepage;

        return $this;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function setContent(?array $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsInHomepage(): ?bool
    {
        return $this->isInHomepage;
    }

    public function setIsInHomepage(bool $isInHomepage): self
    {
        $this->isInHomepage = $this->isHomepage ? true : $isInHomepage;

        return $this;
    }

    public function getHomepageOrder(): ?int
    {
        return $this->homepageOrder;
    }

    public function setHomepageOrder(?int $homepageOrder): self
    {
        $this->homepageOrder = $homepageOrder;

        return $this;
    }

    public function getSortingOrder(): ?int
    {
        return $this->sortingOrder;
    }

    public function setSortingOrder(?int $sortingOrder): self
    {
        $this->sortingOrder = $sortingOrder;

        return $this;
    }

    public function getIsEmptyNode(): ?bool
    {
        return $this->isEmptyNode;
    }

    public function setIsEmptyNode(bool $isEmptyNode): self
    {
        $this->isEmptyNode = $isEmptyNode;

        return $this;
    }

    /**
     * @return Collection|PageAsset[]
     */
    public function getPageAssets(): Collection
    {
        return $this->pageAssets;
    }

    public function addPageAsset(PageAsset $pageAsset): self
    {
        if (!$this->pageAssets->contains($pageAsset)) {
            $this->pageAssets[] = $pageAsset;
            $pageAsset->setPage($this);
        }

        return $this;
    }

    public function removePageAsset(PageAsset $pageAsset): self
    {
        if ($this->pageAssets->removeElement($pageAsset)) {
            // set the owning side to null (unless already changed)
            if ($pageAsset->getPage() === $this) {
                $pageAsset->setPage(null);
            }
        }

        return $this;
    }

    public function getHeader(): ?CroppedAsset
    {
        return $this->header;
    }

    public function setHeader(?CroppedAsset $header): self
    {
        $this->header = $header;

        return $this;
    }

    public function getLvl(): int
    {
        return $this->lvl;
    }
}
