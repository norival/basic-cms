<?php

namespace App\Entity;

use App\Repository\AssetRepository;
use App\Service\FileUploader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=AssetRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Asset
{
    public const ALLOWED_TYPES = [
        'image' => 0,
        'document' => 1,
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("media_collection")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("media_collection")
     */
    private $path;

    /**
     * @ORM\Column(type="smallint")
     * @Groups("media_collection")
     */
    private $type;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups("media_collection")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="assets")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("media_collection")
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("media_collection")
     */
    private $caption;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=CroppedAsset::class, mappedBy="asset", orphanRemoval=true)
     */
    private $croppedAssets;

    /**
     * @ORM\OneToMany(targetEntity=PageAsset::class, mappedBy="asset", orphanRemoval=true)
     */
    private $pageAssets;

    public function __construct()
    {
        $this->croppedAssets = new ArrayCollection();
        $this->pageAssets = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCaption(): ?string
    {
        return $this->caption;
    }

    public function setCaption(?string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return Collection|CroppedAsset[]
     */
    public function getCroppedAssets(): Collection
    {
        return $this->croppedAssets;
    }

    public function addCroppedAsset(CroppedAsset $croppedAsset): self
    {
        if (!$this->croppedAssets->contains($croppedAsset)) {
            $this->croppedAssets[] = $croppedAsset;
            $croppedAsset->setAsset($this);
        }

        return $this;
    }

    public function removeCroppedAsset(CroppedAsset $croppedAsset): self
    {
        if ($this->croppedAssets->removeElement($croppedAsset)) {
            // set the owning side to null (unless already changed)
            if ($croppedAsset->getAsset() === $this) {
                $croppedAsset->setAsset(null);
            }
        }

        return $this;
    }

    /**
     * @Groups("media_collection")
     */
    public function getPublicPath(): string
    {
        return FileUploader::ASSET_PATH . $this->getPath();
    }

    /**
     * @return Collection|PageAsset[]
     */
    public function getPageAssets(): Collection
    {
        return $this->pageAssets;
    }

    public function addPageAsset(PageAsset $pageAsset): self
    {
        if (!$this->pageAssets->contains($pageAsset)) {
            $this->pageAssets[] = $pageAsset;
            $pageAsset->setAsset($this);
        }

        return $this;
    }

    public function removePageAsset(PageAsset $pageAsset): self
    {
        if ($this->pageAssets->removeElement($pageAsset)) {
            // set the owning side to null (unless already changed)
            if ($pageAsset->getAsset() === $this) {
                $pageAsset->setAsset(null);
            }
        }

        return $this;
    }
}
