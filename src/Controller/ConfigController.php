<?php

namespace App\Controller;

use App\Form\ConfigType;
use App\Repository\ConfigRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConfigController extends AbstractController
{
    public function __construct(
        private ConfigRepository $configRepository,
        private EntityManagerInterface $em,
        private FileUploader $fileUploader
    ) { }

    #[Route('/admin/config', name: 'admin_config_edit', methods: ['POST', 'GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(Request $request): Response
    {
        /** @var \App\Entity\Config $config */
        $config = $this->configRepository->findOneBy([]);

        $form = $this->createForm(ConfigType::class, $config);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile|null $file */
            $file = $form->get('siteLogo')->getData();

            if ($file) {
                try {
                    $upload = $this->fileUploader->upload($file);
                } catch (FileException $e) {
                    return $this->renderForm('admin/config/edit.html.twig', [
                        'form' => $form,
                        'config' => $config,
                    ]);
                }

                $config->setSiteLogo($upload['filename']);
            }

            $this->em->flush();

            return $this->redirectToRoute('admin_config_edit');
        }

        return $this->renderForm('admin/config/edit.html.twig', [
            'form' => $form,
            'config' => $config,
        ]);
    }
}
