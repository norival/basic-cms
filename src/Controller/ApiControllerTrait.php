<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;

trait ApiControllerTrait
{
    /**
     * Get a token from the request
     *
     * @throws \Exception
     */
    private function getTokenFromRequest(Request $request): string
    {
        if ($request->get('csrfToken')) {
            return $request->get('csrfToken');
        }

        $content = \json_decode($request->getContent(), true);
        if (!is_array($content) || !array_key_exists('csrfToken', $content)) {
            throw new \Exception('Impossible to find token in the request');
        }

        return $content['csrfToken'];
    }
}
