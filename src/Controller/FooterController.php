<?php

namespace App\Controller;

use App\Repository\ConfigRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FooterController extends AbstractController
{
    public function __construct(
        private ConfigRepository $configRepository
    ) { }

    public function pageFooter(): Response
    {
        /** @var \App\Entity\Config $config */
        $config = $this->configRepository->findOneBy([]);

        return $this->render('footer/page.html.twig', [
            'config' => $config,
        ]);
    }
}
