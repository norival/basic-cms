<?php

namespace App\Controller;

use App\Entity\Asset;
use App\Entity\Contact;
use App\Entity\CroppedAsset;
use App\Entity\Page;
use App\Form\ContactType;
use App\Form\NewPageType;
use App\Form\NodeType;
use App\Form\PageType;
use App\Repository\AssetRepository;
use App\Repository\PageRepository;
use App\Service\ContactHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PageController extends AbstractController
{
    use ApiControllerTrait;

    public function __construct(
        private AssetRepository $assetRepository,
        private ContactHelper $contactHelper,
        private EntityManagerInterface $em,
        private PageRepository $pageRepository,
        private ParameterBagInterface $parameters,
        private UrlGeneratorInterface $urlGenerator
    ) { }

    #[Route('/', name: 'page_home')]
    public function home(
        Request $request
    ): Response {
        /* $this->addFlash('success', 'lorem ipsum dolor sit amet'); */
        /* $this->addFlash('success', 'lorem ipsum dolor sit amet'); */
        /* $this->addFlash('success', 'lorem ipsum dolor sit amet'); */
        /* $this->addFlash('danger', 'lorem ipsum dolor sit amet'); */
        /* $this->addFlash('danger', 'lorem ipsum dolor sit amet'); */
        /* $this->addFlash('warning', 'lorem ipsum dolor sit amet'); */

        $homepage = $this->pageRepository->findOneBy(['isHomepage' => true]);
        $pages = $this->pageRepository->findBy(
            [
                'isDeleted'    => false,
                'isDraft'      => false,
                'isInHomepage' => true,
                'isHomepage'   => false,
                'isEmptyNode'  => false,
            ],
            [
                'homepageOrder' => 'ASC',
            ]
        );

        $contact = new Contact();
        $contactForm = $this->createForm(ContactType::class, $contact);

        $contactForm->handleRequest($request);
        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $result = $this->contactHelper->handleContact(
                $request->get('frc-captcha-solution'),
                $contact
            );

            if ($result['isSuccess']) {
                $this->addFlash(
                    'success', 
                    $result['message']
                );

                return $this->redirectToRoute('page_home');
            }

            // set flash message if email could not be sent
            $this->addFlash(
                'error', 
                $result['message']
            );
        }

        return $this->renderForm('page/homepage.html.twig', [
            'contactForm' => $contactForm,
            'page'        => $homepage,
            'webPages'    => $pages,
        ]);
    }

    #[Route('/{slug}', name: 'page_show')]
    public function show(Page $page): RedirectResponse|Response
    {
        // redirect to home page
        if ($page->getIsHomepage()) {
            return $this->redirectToRoute('page_home');
        }

        if ($page->getIsEmptyNode()) {
            /** @var \Gedmo\Tree\Entity\Repository\NestedTreeRepository $repo */
            $repo = $this->getDoctrine()->getManager()->getRepository(Page::class);
            $children = $repo->children($page);

            if (empty($children)) {
                return $this->redirectToRoute('page_home');
            }

            return $this->redirectToRoute('page_show', [
                'slug' => $children[0]->getSlug(),
            ]);
        }

        return $this->render('page/index.html.twig', [
            'page' => $page,
        ]);
    }

    #[Route('/page/{id}', name: 'page_show_permalink')]
    public function showPermalink(Page $page): RedirectResponse
    {
        // redirect to home page
        if ($page->getIsHomepage()) {
            return $this->redirectToRoute('page_home');
        }

        // redirect to page with slug
        return $this->redirectToRoute('page_show', ['slug' => $page->getSlug()]);
    }

    /*
     * ADMIN ROUTES ------------------------------------------------------------
     */

    #[Route('/admin/pages', name: 'admin_page_list')]
    #[IsGranted('ROLE_ADMIN')]
    public function pages(PageRepository $pageRepository, Request $request): Response
    {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('p')
            ->from('App\Entity\Page', 'p')
            ->where("p.title != '_root'")
            ->orderBy("p.root, p.lft, p.sortingOrder", "ASC")
        ;

        $pages = $qb->getQuery()->getResult();

        return $this->render('admin/pages/index.html.twig', [
            'pages' => $pages,
            'tab' => $request->get('tab'),
        ]);
    }

    #[Route('/admin/node/edit/{id}', name: 'admin_node_edit', methods: ['GET', 'POST'])]
    public function editNode(Request $request, Page $node): Response
    {
        $form = $this->createForm(NodeType::class, $node);
        $convertForm = $this
            ->createFormBuilder($node)
            ->add('convert', SubmitType::class, [
                'label' => "Convertir en page",
            ])
            ->getForm()
        ;

        $convertForm->handleRequest($request);
        if ($convertForm->isSubmitted() && $convertForm->isValid()) {
            $node->setIsEmptyNode(false);
            $this->em->flush();

            return $this->redirectToRoute('admin_page_edit', ['id' => $node->getId()]);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($node);
            $this->em->flush();

            return $this->redirectToRoute('admin_page_list');
        }

        return $this->renderForm('admin/pages/edit_node.html.twig', [
            'convertForm' => $convertForm,
            'form' => $form,
        ]);
    }

    #[Route('/admin/page/edit/{id}', name: 'admin_page_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Page $page): JsonResponse|Response
    {
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->createForm(PageType::class, $page, [
            'is_homepage' => $page->getIsHomepage() ?? false,
            'is_edit' => true,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $page->setUpdatedAt(new \DateTime());

            if ($form->getClickedButton()->getName() === 'convert') {
                $page->setIsEmptyNode(true);
                $this->em->persist($page);
                $this->em->flush();

                return $this->redirectToRoute('admin_page_list');
            }

            // persist page
            $this->em->persist($page);
            $this->em->flush();

            return $this->json('ok');
        }

        return $this->renderForm('admin/pages/edit.html.twig', [
            'form'          => $form,
            'page'          => $page,
            'isEdit'        => true,
            'isHomepage'    => $page->getIsHomepage() ?? false,
            'imagesPerPage' => $this->parameters->get('app.image.per_page'),
        ]);
    }

    #[Route('/admin/page/new', name: 'admin_page_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $page = new Page();
        $form = $this->createForm(NewPageType::class, $page);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $page->setParent($this->pageRepository->findOneBy(['title' => '_root']));
            $page->setUser($this->getUser());

            // persist page
            $this->em->persist($page);
            $this->em->flush();

            return $this->redirectToRoute('admin_page_edit', ['id' => $page->getId()]);
        }

        return $this->renderForm('admin/pages/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/admin/page/sort/{id}', name: 'admin_page_sort', methods: 'POST')]
    public function sort(Request $request, Page $page): JsonResponse
    {
        $content = json_decode($request->getContent(), true);

        /** @var \Gedmo\Tree\Entity\Repository\NestedTreeRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository(Page::class);

        /** @var Page $root */
        $root = $this->pageRepository->findOneBy(['title' => '_root']);

        if ($content['parent']) {
            /** @var Page $parent */
            $parent = $this->pageRepository->find($content['parent']);
            $page->setParent($parent);
        } else {
            $page->setParent($root);
        }

        /** @var Page[] $children */
        $children = $this->pageRepository->findBy(['isDeleted' => false]);
        foreach ($children as $child) {
            if ($child->getTitle() === '_root') {
                continue;
            }

            $child->setSortingOrder($content['order'][$child->getId()]);
        }
        $this->em->flush();

        // reorder left and right values
        $repo->reorder($root, 'sortingOrder');
        $this->em->flush();

        return $this->json('ok');
    }

    #[Route('/api/page/{id}/add-header/{asset_id}', name: 'api_page_add_header', methods: 'POST')]
    #[Entity('asset', expr: 'repository.find(asset_id)')]
    public function addHeader(
        Page $page,
        Asset $asset,
        Request $request
    ): JsonResponse {
        try {
            $token = $this->getTokenFromRequest($request);
        } catch (\Exception $e) {
            return $this->json('Invalid CSRF token', 403);
        }

        if (!$this->isCsrfTokenValid('page-edit', $token)) {
            return $this->json('Invalid CSRF token', 403);
        }

        // create full crop
        $croppedHeader = new CroppedAsset();
        $croppedHeader->setAsset($asset);
        $croppedHeader->setPath($asset->getPath());

        $page->setHeader($croppedHeader);
        $this->em->persist($page);
        $this->em->flush();

        return $this->json('ok');
    }

    #[Route('/api/page/{id}/remove-header', name: 'api_page_remove_header', methods: 'POST')]
    public function removeHeader(Page $page, Request $request): JsonResponse
    {
        try {
            $token = $this->getTokenFromRequest($request);
        } catch (\Exception $e) {
            return $this->json('Invalid CSRF token', 403);
        }

        if (!$this->isCsrfTokenValid('page-edit', $token)) {
            return $this->json('Invalid CSRF token', 403);
        }

        $page->setHeader(null);
        $this->em->persist($page);
        $this->em->flush();

        return $this->json('ok');
    }

    #[Route('/admin/page/delete/{id}', name: 'admin_page_delete', methods: 'POST')]
    public function delete(Page $page, Request $request): RedirectResponse
    {
        $tab = 'active';
        if ($page->getIsDraft()) {
            $tab = 'draft';
        }

        if ($page->getIsDeleted()) {
            $tab = 'deleted';
        }

        try {
            $token = $this->getTokenFromRequest($request);
        } catch (\Exception $e) {
            $this->addFlash(
                'danger',
                'Impossible de supprimer la page'
            );

            return $this->redirectToRoute('admin_page_list', [
                'tab' => $tab,
            ]);
        }

        if (!$this->isCsrfTokenValid('page-delete', $token)) {
            $this->addFlash(
                'danger',
                'Impossible de supprimer la page'
            );

            return $this->redirectToRoute('admin_page_list', [
                'tab' => $tab,
            ]);
        }

        /** @var \Gedmo\Tree\Entity\Repository\NestedTreeRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository(Page::class);
        $children = $repo->children($page, true);

        foreach ($children as $child) {
            $child->setParent($page->getParent());
            $this->em->persist($child);
        }

        if ($page->getIsDeleted()) {
            $this->em->remove($page);
            $this->em->flush();

            $this->addFlash(
                'success',
                'La page a été définitivement supprimée'
            );

            return $this->redirectToRoute('admin_page_list', [
                'tab' => $tab,
            ]);
        }

        $page->setIsDeleted(true);
        $this->em->persist($page);
        $this->em->flush();

        $this->addFlash(
            'success',
            'La page a été placée dans la corbeille'
        );

        return $this->redirectToRoute('admin_page_list', [
            'tab' => $tab,
        ]);
    }

    #[Route('/admin/page/publish/{id}', name: 'admin_page_publish', methods: 'POST')]
    public function publish(Page $page, Request $request): RedirectResponse
    {
        try {
            $token = $this->getTokenFromRequest($request);
        } catch (\Exception $e) {
            $this->addFlash(
                'danger',
                'Impossible de publier la page'
            );

            return $this->redirectToRoute('admin_page_list', [
                'tab' => 'draft',
            ]);
        }

        if (!$this->isCsrfTokenValid('page-publish', $token)) {
            $this->addFlash(
                'danger',
                'Impossible de publier la page'
            );

            return $this->redirectToRoute('admin_page_list', [
                'tab' => 'draft',
            ]);
        }

        $page->setIsDraft(false);
        $this->em->persist($page);
        $this->em->flush();

        $this->addFlash(
            'success',
            'La page a été publiée'
        );

        return $this->redirectToRoute('admin_page_list', [
            'tab' => 'draft',
        ]);
    }

    #[Route('/admin/page/restore/{id}', name: 'admin_page_restore', methods: 'POST')]
    public function restore(Page $page, Request $request): RedirectResponse
    {
        try {
            $token = $this->getTokenFromRequest($request);
        } catch (\Exception $e) {
            $this->addFlash(
                'danger',
                'Impossible de restaurer la page'
            );

            return $this->redirectToRoute('admin_page_list', [
                'tab' => 'deleted',
            ]);
        }

        if (!$this->isCsrfTokenValid('page-publish', $token)) {
            $this->addFlash(
                'danger',
                'Impossible de restaurer la page'
            );

            return $this->redirectToRoute('admin_page_list', [
                'tab' => 'deleted',
            ]);
        }

        $page->setIsDeleted(false);
        $this->em->persist($page);
        $this->em->flush();

        $this->addFlash(
            'success',
            'La page a été restaurée'
        );

        return $this->redirectToRoute('admin_page_list', [
            'tab' => 'deleted',
        ]);
    }

    #[Route('/admin/node/{id}/add-child', name: 'admin_node_add', methods: 'POST')]
    public function addNode(Page $parent, Request $request): JsonResponse
    {
        $content = \json_decode($request->getContent(), true);

        if (
            !array_key_exists('title', $content)
            || !array_key_exists('slug', $content)
        ) {
            return $this->json(
                'Impossible de créer un lien',
                422
            );
        }

        $page = new Page();
        $page->setParent($parent);
        $page->setTitle($content['title']);
        $page->setSlug($content['slug']);
        $page->setIsEmptyNode(true);
        $page->setIsDraft(false);
        $page->setIsInHomepage($parent->getIsInHomepage());

        // put in same menus as parent node
        foreach ($parent->getMenus() as $menu) {
            $menu->addPage($page);
        }

        $this->em->persist($page);
        $this->em->flush();

        return $this->json([
            'path' => $this->urlGenerator->generate('admin_sitemap'),
        ]);
    }
}
