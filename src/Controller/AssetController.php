<?php

namespace App\Controller;

use App\Entity\Asset;
use App\Entity\CroppedAsset;
use App\Entity\Page;
use App\Entity\PageAsset;
use App\Form\AssetType;
use App\Repository\AssetRepository;
use App\Repository\PageAssetRepository;
use App\Repository\PageRepository;
use App\Service\FileUploader;
use App\Service\ImageCropper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AssetController extends AbstractController
{
    public function __construct(
        private AssetRepository $repo,
        private EntityManagerInterface $em,
        private FileUploader $fileUploader,
        private ImageCropper $imageCropper,
        private PageRepository $pageRepository,
        private PageAssetRepository $pageAssetRepository,
        private ParameterBagInterface $params,
        private SerializerInterface $serializer
    ) { }

    #[Route('/asset/{path}', name: 'asset_show_public')]
    #[ParamConverter('asset', options: ['mapping' => ['path' => 'path']])]
    public function showPublic(Asset $asset): BinaryFileResponse
    {
        return new BinaryFileResponse($asset->getPublicPath());
    }

    /*
     * ADMIN ROUTES ------------------------------------------------------------
     */

    #[Route('/api/asset/{type}', name: 'api_asset_fetch_all')]
    #[IsGranted('ROLE_ADMIN')]
    public function apiFetchAll(string|null $type = null): JsonResponse
    {
        if ($type) {
            $assets = $this->repo->findBy(['type' => Asset::ALLOWED_TYPES[$type]]);
        } else {
            $assets = $this->repo->findAll();
        }

        return $this->json($assets, context: [
            'groups' => ['media_collection'],
        ]);
    }

    #[Route('/api/asset/fetch/lazy', name: 'api_asset_fetch_lazy')]
    #[IsGranted('ROLE_ADMIN')]
    public function apiFetch(Request $request): JsonResponse
    {
        $isEmbed = $request->get('isEmbed');
        $templateName = $isEmbed == 'true'
            ? 'admin/assets/_image_collection_frame.html.twig'
            : 'admin/assets/_frame.html.twig';

        $offset = $request->get('offset') ?? 0;
        $perPage = $request->get('perPage') ?? $this->params->get('app.image.per_page');

        /** @var \App\Entity\Asset[] $assets */
        $assets = $this->repo->findBy(
            [
                'type' => Asset::ALLOWED_TYPES['image'],
            ],
            orderBy: ['createdAt' => 'DESC'],
            limit: $perPage,
            offset: $offset
        );
        $nAssets = $this->repo->count([]);

        $htmlAssets = array_reduce(
            $assets,
            fn($html, $asset) => $html . $this->renderView(
                $templateName,
                [
                    'asset' => $asset,
                    'image' => $asset,
                    'type' => 'image',
                ]
            ),
            ''
        );

        return $this->json([
            'content' => $htmlAssets,
            'offset' => (int)$offset + (int)$perPage,
            'perPage' => (int)$perPage,
            'isLastPage' => (int)($offset + $perPage) >= $nAssets,
        ]);
    }

    #[Route('/api/asset/crop/{id}/{cropped_id}/edit', name: 'admin_asset_crop_edit')]
    #[Route('/api/asset/crop/{id}', name: 'admin_asset_crop_new')]
    #[Route('/api/header/{page_id}/crop/{id}/{cropped_id}/edit', name: 'admin_header_crop_edit')]
    #[Route('/api/header/{page_id}/crop/{id}', name: 'admin_header_crop_new')]
    #[Entity('cropped', options: ['id' => 'cropped_id'])]
    #[Entity('page', options: ['id' => 'page_id'])]
    #[IsGranted('ROLE_ADMIN')]
    public function crop(
        Asset $asset,
        Request $request,
        CroppedAsset $cropped = null,
        Page $page = null
    ): JsonResponse {
        if (!$cropped) {
            $cropped = new CroppedAsset();
            $cropped->setAsset($asset);
        }

        // get cropping parameters
        $data = \json_decode($request->getContent(), true);
        $rect = $data['cropData'];

        $filename = "{$this->params->get('assets_directory')}/{$asset->getPath()}";
        $path = $this->imageCropper->crop($filename, $rect);

        if ($cropped->getPath()) {
            // remove old cropped file
            unlink("{$this->params->get('assets_directory')}/{$cropped->getPath()}");
        }

        // set the path
        $cropped->setPath($path);

        if ($page) {
            $page->setHeader($cropped);
        }

        $this->em->persist($cropped);
        $this->em->flush();

        return $this->json($cropped, context: [
            'groups' => ['media_collection'],
        ]);
    }

    #[Route('/admin/assets', name: 'admin_asset_list')]
    public function index(): Response
    {
        $nImages = $this->repo->count([
            'type' => Asset::ALLOWED_TYPES['image'],
        ]);
        $nDocuments = $this->repo->count([
            'type' => Asset::ALLOWED_TYPES['document'],
        ]);

        $documents = $this->repo->findBy(
            [
                'type' => Asset::ALLOWED_TYPES['document'],
            ],
            [
                'createdAt' => 'DESC'
            ],
            limit: $this->params->get('app.image.per_page')
        );

        return $this->render('admin/assets/index.html.twig', [
            'nImages' => $nImages,
            'nDocuments' => $nDocuments,
            'documents' => $documents,
            'types' => Asset::ALLOWED_TYPES,
            'imagesPerPage' => $this->params->get('app.image.per_page') * 2,
        ]);
    }

    #[Route('/admin/asset/upload', name: 'admin_asset_upload')]
    public function upload(Request $request, FileUploader $fileUploader): JsonResponse
    {
        /** @var UploadedFile|null $file */
        $file = $request->files->get('media');

        if (!$file) {
            return $this->json('nothing', 400);
        }

        try {
            $upload = $fileUploader->upload($file);
        } catch (FileException $e) {
            return $this->json('error uploading file', 500);
        }

        $asset = new Asset();
        $asset->setPath($upload['filename']);
        $asset->setType($upload['type']);
        $asset->setTitle(strip_tags($file->getClientOriginalName()));
        $asset->setUser($this->getUser());

        $this->em->persist($asset);
        $this->em->flush();

        switch ($asset->getType()) {
            case Asset::ALLOWED_TYPES['image']:
                $type = 'image';
                break;
            case Asset::ALLOWED_TYPES['document']:
                $type = 'document';
                break;
            default:
                $type = 'image';
        }

        return $this->json([
            'type' => $type,
            'content' => $this->renderView("admin/assets/_frame.html.twig", [
                'asset' => $asset,
                'type' => $type,
            ]),
        ]);
    }

    #[Route('/api/asset/editor/{id}/upload', name: 'api_editor_upload_asset')]
    #[IsGranted('ROLE_ADMIN')]
    public function uploadEditor(
        Page $page, 
        Request $request,
        FileUploader $fileUploader
    ): JsonResponse {
        /** @var UploadedFile|null $file */
        $file = $request->files->get('media');

        if (!$file) {
            return $this->json('nothing', 400);
        }

        try {
            $upload = $fileUploader->upload($file);
        } catch (FileException $e) {
            return $this->json('error uploading file', 500);
        }

        $asset = new Asset();
        $asset->setPath($upload['filename']);
        $asset->setType($upload['type']);
        $asset->setTitle(strip_tags($file->getClientOriginalName()));
        $asset->setUser($this->getUser());

        $pageAsset = new PageAsset();
        $pageAsset->setPage($page);
        $pageAsset->setAsset($asset);

        $this->em->persist($pageAsset);
        $this->em->persist($asset);
        $this->em->flush();

        return $this->json(
            [
                'asset' => $asset,
                'pageAssetId' => $pageAsset->getId(),
                'renderedAsset' => $this->renderView(
                    'admin/assets/_image_collection_frame.html.twig',
                    [
                        'image' => $asset,
                    ]
                ),
            ],
            context: [
                'groups' => ['media_collection'],
            ]
        );
    }

    #[Route('/admin/asset/delete/{id}', name: 'admin_asset_delete')]
    public function delete(Asset $asset): JsonResponse
    {
        $assetId = $asset->getId();
        $assetType = $asset->getType();

        try {
            $this->fileUploader->remove($asset->getPath());

            $this->em->remove($asset);
            $this->em->flush();
        } catch (IOException $e) {
            return $this->json($e->getMessage(), 500);
        }

        return $this->json([
            'id' => $assetId,
            'type' => $assetType,
        ]);
    }

    #[Route('/admin/asset/edit/{id}', name: 'admin_asset_edit')]
    public function edit(Request $request, Asset $asset): Response
    {
        $form = $this->createForm(AssetType::class, $asset);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('admin_asset_list');
        }

        switch ($asset->getType()) {
            case Asset::ALLOWED_TYPES['image']:
                $type = 'image';
                break;
            case Asset::ALLOWED_TYPES['document']:
                $type = 'document';
                break;
            default:
                $type = 'image';
        }

        return $this->renderForm('admin/assets/edit.html.twig', [
            'asset' => $asset,
            'form' => $form,
            'type' => $type,
        ]);
    }

    #[Route('/admin/asset/{id}/pages', name: 'admin_asset_pages')]
    public function getPages(Asset $asset): JsonResponse
    {
        /** @var \App\Entity\PageAsset[] $pageAssets */
        $pageAssets = $this->pageAssetRepository->findBy(['asset' => $asset]);

        /** @var \App\Entity\Page[] $pagesWithHeader */
        $pagesWithHeader = $this->pageRepository->findBy(['header' => $asset]);

        $pages = array_merge(
            array_map(
                fn($pageAsset) => $pageAsset->getPage()->getTitle(),
                $pageAssets
            ),
            array_map(
                fn($page) => $page->getTitle(),
                $pagesWithHeader
            ),
        );

        return $this->json($pages);
    }

    #[Route('/admin/asset/{id}/replace', name: 'admin_asset_replace', methods: "POST")]
    public function replace(
        Asset $asset,
        Request $request,
        FileUploader $fileUploader
    ): JsonResponse {
        /** @var \App\Entity\PageAsset[] $pageAssets */
        $pageAssets = $this->pageAssetRepository->findBy(['asset' => $asset]);

        /** @var UploadedFile|null $file */
        $file = $request->files->get('media');

        if (!$file) {
            return $this->json('nothing', 400);
        }

        try {
            $upload = $fileUploader->replace($asset, $file);
        } catch (FileException $e) {
            return $this->json('error uploading file', 500);
        } catch (IOException $e) {
            return $this->json('error deleting old file', 500);
        }

        $asset->setPath($upload['filename']);
        $this->em->flush();

        return $this->json([
            'path' => $this->generateUrl('admin_asset_edit', ['id' => $asset->getId()])
        ]);
    }
}
