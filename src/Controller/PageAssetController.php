<?php

namespace App\Controller;

use App\Entity\Asset;
use App\Entity\Page;
use App\Entity\PageAsset;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageAssetController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    ) { }

    #[Route('/api/page-asset/add/{page_id}/{asset_id}', name: 'api_editor_add_asset_to_page')]
    #[IsGranted('ROLE_ADMIN')]
    #[Entity('asset', expr: 'repository.find(asset_id)')]
    #[Entity('page', expr: 'repository.find(page_id)')]
    public function addToPage(
        Asset $asset, 
        Page $page,
        Request $request
    ): JsonResponse {
        $pageAsset = new PageAsset();
        $pageAsset->setPage($page);
        $pageAsset->setAsset($asset);

        $this->em->persist($pageAsset);
        $this->em->flush();

        return $this->json($pageAsset->getId());
    }

    #[Route('/api/page-asset/{id}/remove', name: 'api_editor_remove_asset_from_page')]
    #[IsGranted('ROLE_ADMIN')]
    public function removeFromPage(
        PageAsset $pageAsset,
        Request $request
    ): JsonResponse {
        $this->em->remove($pageAsset);
        $this->em->flush();

        return $this->json('ok');
    }
}
