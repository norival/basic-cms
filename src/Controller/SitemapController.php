<?php

namespace App\Controller;

use App\Entity\Page;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private PageRepository $pageRepository
    ) { }

    #[Route('/sitemap.xml', name: 'sitemap', defaults: ['_format' => 'xml'], methods: ['GET'], priority: 100)]
    public function show() {
        return $this->render('sitemap.xml.twig', [
            'pages' => $this->pageRepository->findActivePages(),
        ]);
    }

    /*
     * ADMIN ROUTES ------------------------------------------------------------
     */

    #[Route('/admin/tree', name: 'admin_sitemap', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function treeView()
    {
        /** @var \Gedmo\Tree\Entity\Repository\NestedTreeRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository(Page::class);

        $qb = $this
            ->em
            ->createQueryBuilder()
            ->select('page')
            ->from('\App\Entity\Page', 'page')
            ->orderBy('page.root, page.lft', 'ASC')
            ->andWhere('page.isDeleted = 0')
            ->andWhere("page.title != '_root'")
        ;

        return $this->render('admin/sitemap/index.html.twig', [
            'tree' => $repo->buildTree($qb->getQuery()->getArrayResult()),
        ]);
    }
}
