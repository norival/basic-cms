<?php

namespace App\Controller;

use App\Entity\Page;
use App\Repository\MenuRepository;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em,
        private MenuRepository $menuRepository,
        private PageRepository $pageRepository
    ) { }

    public function get(string $name): Response
    {
        return $this->render('menu/menu.html.twig', [
            'menu' => $this->menuRepository->findAsTreeView($name),
        ]);
    }

    /*
     * ADMIN ROUTES ------------------------------------------------------------
     */

    #[Route('/admin/menus', name: 'admin_menu_list')]
    #[IsGranted('ROLE_ADMIN')]
    public function menus(): Response
    {
        /** @var \App\Entity\Menu[] $menus */
        $menus = $this->menuRepository->findAll();

        /** @var \Gedmo\Tree\Entity\Repository\NestedTreeRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository(Page::class);

        $trees = [];
        foreach ($menus as $menu) {
            $qb = $this
                ->em
                ->createQueryBuilder()
                ->select('page')
                ->from('\App\Entity\Page', 'page')
                ->leftJoin('page.menus', 'menu')
                ->orderBy('page.root, page.lft', 'ASC')
                ->where('menu = :menu')
                ->andWhere('page.isDraft = 0')
                ->andWhere('page.isDeleted = 0')
                ->setParameter(':menu', $menu)
            ;
            $trees[] = $repo->buildTree(
                $qb->getQuery()
                   ->getArrayResult()
            );
        }

        return $this->render('admin/menus/index.html.twig', [
            'menus' => $menus,
            'pages' => $this->pageRepository->findBy(['isDeleted' => false]),
            'trees' => $trees,
        ]);
    }

    #[Route('/admin/menu/new', name: 'admin_menu_new')]
    #[IsGranted('ROLE_ADMIN')]
    public function new(): Response
    {
        return $this->render('admin/menus/index.html.twig', [
        ]);
    }
}
