<?php

namespace App\Controller;

use App\Repository\AssetRepository;
use App\Repository\PageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    public function __construct(
        private AssetRepository $assetRepository,
        private PageRepository $pageRepository
    ) { }

    /**
     * Search from api
     */
    #[Route('/api/search', name: 'api_search', methods: 'GET')]
    #[IsGranted('ROLE_ADMIN')]
    public function index(Request $request): JsonResponse
    {
        $search = $request->get('search');
        $items = [];

        /** @var \App\Entity\Asset[] $assets */
        $assets = $this->assetRepository->search($search);
        $assets = array_map(
            fn($asset) => [
                'href' => $this->generateUrl('asset_show_public', ['path' => $asset->getPath()]),
                'name' => "Média: {$asset->getTitle()}",
            ],
            $assets
        );

        /** @var \App\Entity\Page[] $pages */
        $pages = $this->pageRepository->search($search);
        $pages = array_map(
            fn($page) => [
                'href' => $this->generateUrl('page_show_permalink', ['id' => $page->getId()]),
                'name' => "Page: {$page->getTitle()}",
            ],
            $pages
        );

        $items = array_merge($items, $assets, $pages);

        return $this->json([
            'success' => true,
            'items' => $items,
        ]);
    }
}
