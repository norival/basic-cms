<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ConfigRepository;
use App\Repository\ContactRepository;
use App\Service\ContactHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ContactController extends AbstractController
{
    public function __construct(
        private ContactHelper $contactHelper,
        private ConfigRepository $configRepository,
        private EntityManagerInterface $em,
        private MailerInterface $mailer,
        private ParameterBagInterface $params,
        private TranslatorInterface $translator
    ) { }

    #[Route('/contact', name: 'contact', priority: 10)]
    public function index(
        Request $request
    ): Response {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->contactHelper->handleContact(
                $request->get('frc-captcha-solution'),
                $contact
            );

            if ($result['isSuccess']) {
                $this->addFlash(
                    'success', 
                    $result['message']
                );

                return $this->redirectToRoute('page_home');
            }

            // set flash message if email could not be sent
            $this->addFlash(
                'error', 
                $result['message']
            );
        }

        return $this->renderForm('contact/index.html.twig', [
            'form' => $form,
        ]);
    }

    /*
     * ADMIN ROUTES ------------------------------------------------------------
     */

    #[Route('/admin/contact/{id}/delete', name: 'admin_contact_delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(Contact $contact): JsonResponse
    {
        $id = $contact->getId();
        $this->em->remove($contact);
        $this->em->flush();

        return $this->json($id, 200);
    }

    #[Route('/admin/contacts', name: 'admin_contact_list')]
    #[IsGranted('ROLE_ADMIN')]
    public function list(ContactRepository $contactRepository): Response
    {
        $contacts = $contactRepository->findBy([], ['createdAt' => 'DESC']);

        return $this->render('admin/contact/index.html.twig', [
            'contacts' => $contacts,
        ]);
    }

    #[Route('/admin/contact/{id}', name: 'admin_contact_show')]
    #[IsGranted('ROLE_ADMIN')]
    public function show(Contact $contact): Response
    {
        return $this->render('admin/contact/_show.html.twig', [
            'contact' => $contact,
        ]);
    }
}
