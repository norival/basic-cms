<?php

namespace App\Form;

use App\Entity\Config;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tbmatuka\EditorjsBundle\Form\EditorjsType;

class ConfigType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator
    ) { }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contactEmail', EmailType::class, [
                'label' => $this->translator->trans('Email de contact'),
                'help' => $this->translator->trans('Il s\'agit de l\'email utilisé dans le formulaire de contact'),
            ])
            ->add('contactName', TextType::class, [
                'label' => $this->translator->trans('Contact'),
            ])
            ->add('contactPhone', TextType::class, [
                'label' => $this->translator->trans('Téléphone de contact'),
            ])
            ->add('contactAddress', TextareaType::class, [
                'label' => $this->translator->trans('Adresse de contact'),
            ])
            ->add('legalNotice', TextareaType::class, [
                'label' => $this->translator->trans('Mentions légales'),
            ])
            ->add('siteDescription', TextareaType::class, [
                'label' => $this->translator->trans('Description du site'),
            ])
            ->add('siteTitle', TextType::class, [
                'label' => $this->translator->trans('Titre du site'),
            ])
            ->add('siteLogo', FileType::class, [
                'label' => $this->translator->trans('Logo du site'),
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '5m',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => $this->translator->trans("Merci d'upload une image"),
                    ])
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => $this->translator->trans('Enregistrer'),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Config::class,
        ]);
    }
}
