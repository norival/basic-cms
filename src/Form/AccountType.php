<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccountType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator
    ) { }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => $this->translator->trans('Adresse email'),
            ])
            ->add('firstname', TextType::class, [
                'label' => $this->translator->trans('Prénom'),
            ])
            ->add('lastname', TextType::class, [
                'label' => $this->translator->trans('Nom'),
            ])
            ->add('nickname', TextType::class, [
                'label' => $this->translator->trans('Nom d\'utilisateur'),
            ])
            ->add('password', ChangePasswordType::class, [
                'label' => $this->translator->trans("Mot de passe"),
                'required' => false,
                'is_embedded' => true,
                'mapped' => false,
                /* 'priority' => -5, */
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
