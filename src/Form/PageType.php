<?php

namespace App\Form;

/* use App\Entity\Block; */
use App\Entity\Menu;
use App\Entity\Page;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tbmatuka\EditorjsBundle\Form\EditorjsType;

class PageType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator
    ) { }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', HiddenType::class, [
                /* 'label' => $this->translator->trans('Titre de la page'), */
                /* 'label' => false, */
                'row_attr' => [
                    'id' => 'pageTitle',
                ],
                'attr' => [
                    'placeholder' => $this->translator->trans('Titre de la page'),
                    'maxLength' => Page::TITLE_LENGTH,
                ],
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'max' => Page::TITLE_LENGTH,
                    ]),
                    new NotBlank(),
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'row_attr' => [
                    'id' => 'pageDescription',
                ],
                'attr' => [
                    'placeholder' => $this->translator->trans('Description courte de la page'),
                ],
                'label' => $this->translator->trans("Description de la page"),
                'help' => $this->translator->trans('Description courte de la page, utilisée pour le rendu de la page d\'accueil et par les moteurs de recherche.'),
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'max' => Page::DESCRIPTION_LENGTH,
                    ]),
                ],
            ])
            ->add('menus', EntityType::class, [
                'class' => Menu::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'label' => $this->translator->trans('Inclure dans les menus :'),
                'by_reference' => false,
            ])
            ->add('slug', TextType::class, [
                'label' => $this->translator->trans("Slug de l'URL"),
                'attr' => [
                    'placeholder' => $this->translator->trans('Slug de la page'),
                ],
            ])
            ->add('content', HiddenType::class)
            ->add('convert', SubmitType::class, [
                'label' => "Convertir en noeud simple",
                'attr' => [
                    'value' => 'convert',
                ],
            ])
        ;

        $builder->get('content')
                ->addModelTransformer(new CallbackTransformer(
                    function ($contentAsArray) {
                        return \json_encode($contentAsArray);
                    },
                    function ($contentAsJson) {
                        return \json_decode($contentAsJson, true);
                    }
                ))
        ;

        if (!$options['is_edit']) {
            $builder
                ->add('save', SubmitType::class, [
                    'label' => $this->translator->trans('Enregistrer la page'),
                ])
            ;
        }

        if (!$options['is_homepage']) {
            // these options are excluded if we edit the homepage
            $builder
                ->add('isDraft', CheckboxType::class, [
                    'required' => false,
                    'row_attr' => [
                        'id' => 'pageIsDraft',
                    ],
                    'label' => $this->translator->trans('La page est un brouillon'),
                    'help' => $this->translator->trans('Tant que la page est enregistrée comme brouillon, elle ne sera pas visible sur le site.'),
                ])
                ->add('isInHomepage', CheckboxType::class, [
                    'required' => false,
                    'label' => $this->translator->trans("Afficher dans la page d'accueil"),
                    'help' => $this->translator->trans('Déchocher cette case si vous ne voulez pas que cette page soit ajoutée automatiquement à la page d\'accueil.'),
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
            'is_edit' => false,
            'is_homepage' => false,
        ]);

        $resolver->setAllowedTypes('is_homepage', 'bool');
    }
}
