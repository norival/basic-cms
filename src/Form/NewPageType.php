<?php

namespace App\Form;

use App\Entity\Menu;
use App\Entity\Page;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class NewPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'label' => "Titre de la page",
                'attr' => [
                    'maxLength' => Page::TITLE_LENGTH,
                ],
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'max' => Page::TITLE_LENGTH
                    ]),
                    new NotBlank(),
                ],
            ])
            ->add('slug', TextType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Regex('/^[a-z0-9]+(?:-[a-z0-9]+)*$/'),
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => "Description de la page",
                'attr' => [
                    'maxLength' => Page::DESCRIPTION_LENGTH,
                ],
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'max' => Page::DESCRIPTION_LENGTH,
                    ]),
                ],
            ])
            ->add('isDraft', CheckboxType::class, [
                'required' => false,
                'label' => "Enregistrer en tant que brouillon ?",
            ])
            ->add('isInHomepage', CheckboxType::class, [
                'required' => false,
                'label' => "La page doit-elle apparaître sur la page d'accueil ?",
            ])
            ->add('menus', EntityType::class, [
                'required' => false,
                'class' => Menu::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'label' => "Dans quels menus la page doit-elle apparaître ?",
                'by_reference' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Commencer à rédiger la page",
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
