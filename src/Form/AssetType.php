<?php

namespace App\Form;

use App\Entity\Asset;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class AssetType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator
    ) { }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'label' => $this->translator->trans('Titre'),
                'help' => $this->translator->trans("Titre de l'image"),
            ])
            ->add('caption', TextareaType::class, [
                'required' => false,
                'label' => $this->translator->trans('Légende'),
                'help' => $this->translator->trans("Utilisée comme texte alternatif pour les liseurs d'écran ou si l'image ne peut pas être chargée"),
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => $this->translator->trans('Description'),
                'help' => $this->translator->trans("Une petite description de l'image"),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Asset::class,
        ]);
    }
}
