<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class ChangePasswordType extends AbstractType
{
    public function __construct(
        private TranslatorInterface $translator
    ) { }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $constraints = [
            new Length([
                'min' => 12,
                'minMessage' => 'Le mot de passe doit faire au moins {{ limit }} caractères',
                'max' => 4096,
            ]),
        ];

        if (!$options['is_embedded']) {
            $constraints[] = new NotBlank([
                'message' => $this->translator->trans('Merci de renseigner un mot de passe'),
            ]);
        }

        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => !$options['is_embedded'],
                'first_options' => [
                    'required' => !$options['is_embedded'],
                    'attr' => ['autocomplete' => 'new-password'],
                    'constraints' => $constraints,
                    'label' => $this->translator->trans('Nouveau mot de passe'),
                ],
                'second_options' => [
                    'required' => !$options['is_embedded'],
                    'attr' => ['autocomplete' => 'new-password'],
                    'label' => $this->translator->trans('Confirmer le mot de passe'),
                ],
                'invalid_message' => $this->translator->trans('Les mots de passe ne correspondent pas'),
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'is_embedded' => false,
        ]);
    }
}
