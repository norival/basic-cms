<?php

namespace App\Twig;

use App\Repository\AssetRepository;
use App\Repository\CroppedAssetRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    const PAGE_BLOCK_DIR = 'page/blocks/';

    public function __construct(
        private AssetRepository $assetRepository,
        private CroppedAssetRepository $croppedAssetRepository,
        private Environment $twig,
        private TranslatorInterface $translator,
        private UrlGeneratorInterface $urlGenerator
    ) { }

    public function getFilters()
    {
        return [
            new TwigFilter('editorjs', [$this, 'renderEditorContent']),
            new TwigFilter('menu', [$this, 'renderMenu']),
            new TwigFilter('str_contains', [$this, 'strContains']),
            new TwigFilter('tree', [$this, 'renderTree']),
        ];
    }

    /**
     * Parse editor's blocks as html
     */
    public function renderEditorContent(Array $content): string
    {
        if (!array_key_exists('blocks', $content)) {
            return '';
        }

        $html = '';

        foreach ($content['blocks'] as $block) {
            switch ($block['type']) {
                case 'paragraph':
                    $html .= "<p class='paragraph'>{$block['data']['text']}</p>";
                    break;
                case 'header':
                    $html .= $this->twig->render('page/blocks/_header.html.twig', [
                        'content' => $block['data']['text'],
                        'level' => $block['data']['level'],
                    ]);
                    break;
                case 'list':
                    $html .= $this->twig->render('page/blocks/_list.html.twig', [
                        'isOrdered' => $block['data']['style'] === 'ordered',
                        'items' => $block['data']['items'],
                    ]);
                    break;
                case 'image':
                    if ($block['data']['crop']) {
                        /** @var \App\Entity\CroppedAsset $asset */
                        $asset = $this->croppedAssetRepository->find($block['data']['crop']['id']);
                    } else {
                        /** @var \App\Entity\Asset $asset */
                        $asset = $this->assetRepository->find($block['data']['id']);
                    }

                    $html .= $this->twig->render('page/blocks/_image.html.twig', [
                        'asset' => $asset,
                        'caption' => $block['data']['caption'],
                    ]);
                    break;
                case 'imageGrid':
                    $html .= $this->twig->render('page/blocks/_image_grid.html.twig', [
                        'colWidth' => 12 / $block['data']['imagesPerLine'],
                        'images' => $block['data']['images'],
                    ]);

                    break;
                case 'imageWrap':
                    switch ($block['data']['layout']) {
                        case 0:
                        case 3:
                            $col1 = 'is-4';
                            $col2 = 'is-8';
                            break;
                        case 2:
                        case 5:
                            $col1 = 'is-8';
                            $col2 = 'is-4';
                            break;
                        default:
                            $col1 = 'is-6';
                            $col2 = 'is-6';
                            break;
                    }

                    $html .= $this->twig->render('page/blocks/_image_wrap.html.twig', [
                        'col1' => $col1,
                        'col2' => $col2,
                        'image' => $block['data']['image'],
                        'imageContent' => in_array($block['data']['layout'], [0, 1, 2]) ? 1 : 2,
                        'content' => $block['data']['text']['content'],
                    ]);
                    break;
                default:
                    throw new \LogicException('Not recognized type: ' . $block['type']);
            }
        }

        return $html;
    }

    /**
     * Adds str_contains filter.
     */
    public function strContains(string $haystack, string $needle): bool
    {
        return str_contains($haystack, $needle);
    }

    /**
     * Render HTML tree from tree array
     */
    public function renderTree(array $tree, array $attributes = [], bool $isTopLevel = true) : string
    {
        $html = '';

        if ($isTopLevel) {
            $html .= '<div class="tree-container">';
        }

        $listAttributes = 
            array_key_exists('list', $attributes)
            ? $this->generateAttributeString($attributes['list'])
            : '';
        $itemAttributes = 
            array_key_exists('node', $attributes)
            ? $this->generateAttributeString($attributes['node'])
            : '';

        $html .= "<ul {$listAttributes}>";
        foreach ($tree as $node) {
            $url = $this->urlGenerator->generate('admin_page_sort', ['id' => $node['id']]);

            if (empty($node['__children'])) {
                $html .= "<li $itemAttributes data-tree-node-id='{$node['id']}' data-sort-url='$url'>
                    {$this->renderTreeNodeContent($node)}
                ";
                if ($node['isDraft']) {
                    $html .= "<span class='tag is-warning'>{$this->translator->trans('Brouillon')}</span>";
                }
                $html .= "<ul $listAttributes></ul></li>";

                continue;
            }

            $html .= "<li $itemAttributes data-tree-node-id='{$node['id']}' data-sort-url='$url'>
                {$this->renderTreeNodeContent($node)}
                {$this->renderTree($node['__children'], $attributes, false)}
            </li>";
        }

        if ($isTopLevel) {
            return "$html</ul></div>";
        }

        return "$html</ul>";
    }

    /**
     * Render a menu from the tree
     *
     * @param  array $tree
     * @param  bool $isTopLevel
     * @return string
     */
    public function renderMenu(array $tree, bool $isTopLevel = true): string
    {
        $html = '';

        foreach ($tree as $node) {
            $url =
                $node['isEmptyNode']
                ? '#'
                : $this->urlGenerator->generate('page_show', ['slug' => $node['slug']]);
            if (empty($node['__children'])) {
                $html .= "<a class='navbar-item' href='$url'>{$node['title']}</a>";

                continue;
            }

            $html .= "<div class='navbar-item has-dropdown is-hoverable'>
                <a class='navbar-link'>{$node['title']}</a>
                <div class='navbar-dropdown is-boxed'>
                    {$this->renderMenu($node['__children'], false)}
            ";
        }

        if ($isTopLevel) {
            return $html;
        }

        return "$html</div></div>";
    }

    private function renderTreeNodeContent(array $node): string
    {
        $url = $this->urlGenerator->generate('admin_node_add', ['id' => $node['id']]);
        $nodeMarkerClass = $node['isEmptyNode'] ? 'empty-node-item' : 'page-item';

        return "
        <span
            class='tree-node-content'
            data-controller='admin--tree'
            data-admin--tree-parent-id-value='{$node['id']}'
            data-admin--tree-add-node-url-value='$url'>
            <span class='node-marker $nodeMarkerClass'></span>
            <span>{$node['title']}</span>
            <button
                data-action='admin--tree#addNode'
                class='tree-insert-node button is-normal static'>
                <span class='icon'>
                    <i class='fas fa-arrow-right'></i>
                </span>
            </button>
        </span>
        ";
    }

    private function generateAttributeString(array $array): string
    {
        return trim(
            implode(
                ' ',
                array_map(
                    fn($val, $key) => "$key='$val'",
                    $array,
                    array_keys($array)
                )
            )
        );
    }
}
