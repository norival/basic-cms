<?php

namespace App\Repository;

use App\Entity\CroppedAsset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CroppedAsset|null find($id, $lockMode = null, $lockVersion = null)
 * @method CroppedAsset|null findOneBy(array $criteria, array $orderBy = null)
 * @method CroppedAsset[]    findAll()
 * @method CroppedAsset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CroppedAssetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CroppedAsset::class);
    }

    // /**
    //  * @return CroppedAsset[] Returns an array of CroppedAsset objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CroppedAsset
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
