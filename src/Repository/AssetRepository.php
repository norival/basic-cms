<?php

namespace App\Repository;

use App\Entity\Asset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Asset|null find($id, $lockMode = null, $lockVersion = null)
 * @method Asset|null findOneBy(array $criteria, array $orderBy = null)
 * @method Asset[]    findAll()
 * @method Asset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Asset::class);
    }

    /**
     * Search all assets
     *
     * @return \App\Entity\Asset[]
     */
    public function search(string $search): array
    {
        return $this
            ->createQueryBuilder('asset')
            ->where("asset.title LIKE :search")
            ->setParameter('search', "%$search%")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
}
