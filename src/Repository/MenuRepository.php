<?php

namespace App\Repository;

use App\Entity\Menu;
use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Menu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Menu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Menu[]    findAll()
 * @method Menu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Menu::class);
    }

    public function findAsTreeView(string $name)
    {
        /** @var \Gedmo\Tree\Entity\Repository\NestedTreeRepository $pageRepo */
        $pageRepo = $this->getEntityManager()->getRepository(Page::class);

        $qb = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('page')
            ->from('\App\Entity\Page', 'page')
            ->leftJoin('page.menus', 'menu')
            ->orderBy('page.root, page.lft', 'ASC')
            ->where('menu.name = :name')
            ->andWhere('page.isDeleted = 0')
            ->andWhere('page.isDraft = 0')
            ->andWhere("page.title != '_root'")
            ->andWhere("page.title != '_root'")
            ->andWhere("page.isHomepage = 0")
            ->setParameter(':name', $name)
        ;

        return $pageRepo->buildTree($qb->getQuery()->getArrayResult());
    }
}
