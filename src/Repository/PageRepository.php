<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /**
     * Search all assets
     *
     * @return \App\Entity\Page[]
     */
    public function findActivePages(): array
    {
        return $this
            ->createQueryBuilder('page')
            ->orderBy('page.root, page.lft', 'ASC')
            ->andWhere('page.isDeleted = 0')
            ->andWhere("page.isDraft = 0")
            ->andWhere("page.isEmptyNode = 0")
            ->andWhere("page.title != '_root'")
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Search all assets
     *
     * @return \App\Entity\Page[]
     */
    public function search(string $search): array
    {
        return $this
            ->createQueryBuilder('page')
            ->where("page.title LIKE :search")
            ->orWhere("page.slug LIKE :search")
            ->andWhere("page.isEmptyNode = false")
            ->setParameter('search', "%$search%")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
}
