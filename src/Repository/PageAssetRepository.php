<?php

namespace App\Repository;

use App\Entity\PageAsset;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PageAsset|null find($id, $lockMode = null, $lockVersion = null)
 * @method PageAsset|null findOneBy(array $criteria, array $orderBy = null)
 * @method PageAsset[]    findAll()
 * @method PageAsset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageAssetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PageAsset::class);
    }

    // /**
    //  * @return PageAsset[] Returns an array of PageAsset objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PageAsset
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
