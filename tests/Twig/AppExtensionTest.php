<?php

namespace App\Tests\Twig;

use App\Twig\AppExtension;
use PHPUnit\Framework\TestCase;

class AppExtensionTest extends TestCase
{
    public function testThatSrcContainsWorks(): void
    {
        $extension = new AppExtension();
        $this->assertFalse($extension->strContains('admin_page', 'admin_page_new'));
        $this->assertTrue($extension->strContains('admin_page_new', 'admin_page'));
    }

    public function testThatBlockRenderingThrowException(): void
    {
        $extension = new AppExtension();

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Content must contain blocks.');
        $extension->renderEditorContent([]);
    }

    public function testThatBlockRenderingWorks(): void
    {
        $extension = new AppExtension();

        // given the data
        $content = [
            'time' => 1625096908029,
            'version' => '2.22.0',
            'blocks' => [
                [
                    'id' => 'utX02rTdcE',
                    'type' => 'header',
                    'data' => [
                        'level' => 3,
                        'text' => 'Title',
                    ],
                ],
                [
                    'id' => 'utX02rTdcE',
                    'type' => 'paragraph',
                    'data' => [
                        'text' => 'Test',
                    ],
                ],
            ],
        ];

        $rendering = $extension->renderEditorContent($content);

        $this->assertIsString($rendering);
        $this->assertTrue(str_contains($rendering, '<p>Test</p>'));
        $this->assertFalse(str_contains($rendering, '<figure>'));
        $this->assertFalse(str_contains($rendering, '<ul>'));
    }
}
