<?php

namespace App\Tests\Backoffice\Menus;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();

        /** @var UserRepository $userRepository */
        $userRepository = static::$container->get(UserRepository::class);
        $admin = $userRepository->findOneBy(['email' => 'xavier@norival.dev']);
        $client->loginUser($admin);

        $crawler = $client->request('GET', '/admin/menus');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Menus');
    }
}
