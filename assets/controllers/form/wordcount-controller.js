import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = [
        'count',
        'input',
        'max',
    ];

    static values = {
        max: Number,
    };

    updateCounter = () => {
        this.countTarget.innerHTML = this.inputTarget.value.trim().length;
    }
}
