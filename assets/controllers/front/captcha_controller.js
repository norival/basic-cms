import { Controller } from 'stimulus';
import { WidgetInstance } from "friendly-challenge";

export default class extends Controller {
    static targets = [
        'submit',
        'widget',
    ];

    connect() {
        this.widget = new WidgetInstance(this.widgetTarget, {
            doneCallback: this._onChallengeDone,
            sitekey: 'FCMJCLQV00VMC8G0',
            startMode: 'focus',
        });
    }

    _onChallengeDone = (solution) => {
        this.submitTarget.removeAttribute('disabled');
    }
}
