import { Controller } from 'stimulus';
import { v4 as uuidv4 } from 'uuid';
const slug = require('slug')

export default class extends Controller {
    slug = '';

    static values = {
    };

    static targets = [
        'link',
        'data',
        'input',
        'output',
    ];

    updateSlug(event) {
        let title = uuidv4();
        if (event.detail?.title) {
            title = event.detail.title;
        } else {
            title = event.target.value;
        }

        this.slug = slug(title);
        this.dataTarget.value = this.slug;
    }
}
