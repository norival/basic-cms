import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
        csrfToken: String,
        documentCount: Number,
        imageCount: Number,
        uploadUrl: String,
    };

    static targets = [
        'dialog',
        'dialogConfirm',
        'documentCollection',
        'documentCount',
        'imageCollection',
        'imageCount',
        'input',
    ];

    upload() {
        if (this.inputTarget.files === null) {
            return;
        }

        const data = new FormData();
        const file = this.inputTarget.files[0];
        data.append('media', file, file.name)

        fetch(this.uploadUrlValue, {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then(data => this._renderPreview(data));
    }

    _renderPreview(asset) {
        if (asset.type === 'image') {
            this.imageCollectionTarget.insertAdjacentHTML('afterbegin', asset.content);
        }

        if (asset.type === 'document') {
            this.documentCollectionTarget.insertAdjacentHTML('afterbegin', asset.content);
        }

        this._increaseCounter(asset.type);
    }

    _increaseCounter(type) {
        if (type === 'document' || type === 1) {
            this.documentCountValue++;
            this.documentCountTarget.innerHTML = this.documentCountValue;
        }

        if (type === 'image' || type === 0) {
            this.imageCountValue++;
            this.imageCountTarget.innerHTML = this.imageCountValue;
        }
    }

    _decreaseCounter(type) {
        if (type === 'document' || type === 1) {
            this.documentCountValue--;
            this.documentCountTarget.innerHTML = this.documentCountValue;
        }

        if (type === 'image' || type === 0) {
            this.imageCountValue--;
            this.imageCountTarget.innerHTML = this.imageCountValue;
        }
    }

    delete(event) {
        event.preventDefault();
        const url = event.currentTarget.dataset.url;
        const pagesUrl = event.currentTarget.dataset.pagesUrl;

        if (url && pagesUrl) {
            fetch(pagesUrl)
                .then(response => response.json())
                .then(pages => {
                    const suppMessage = this.dialogTarget.querySelector('.modal-body-supp-message');
                    if (pages.length > 0) {
                        const pageItems = pages.reduce((accu, page) => `${accu}<li>${page}</li>`, '');
                        suppMessage.innerHTML = `
                            <span class="icon-text">
                                <span class="icon">
                                    <i class="fas fa-exclamation-triangle"></i>
                                </span>
                                <span>Attention, ce média est présent dans les pages suivantes :</span>
                            </span>
                            <ul>
                                ${pageItems}
                            </ul>
                            <p>Si vous le supprimez, il sera supprimé de ces pages !</p>
                        `;
                    } else {
                        suppMessage.innerHTML = `
                            <span class="icon-text">
                                <span class="icon">
                                    <i class="fas fa-check"></i>
                                </span>
                                <span>Ce média n'est présent sur aucune page. Vous pouvez le supprimer librement.</span>
                            </span>
                        `;
                    }

                    this.dialogTarget.dataset.url = url;
                    this.dialogTarget.classList.add('is-active');
                });
        }
    }

    confirm() {
        const url = this.dialogTarget.dataset.url;
        this.dialogConfirmTarget.classList.add('is-loading');

        fetch(url, {
            method: 'DELETE',
        })
            .then(response => response.json())
            .then(asset => {
                const deletedAsset = this.element.querySelector(`.asset-card[data-asset-id="${asset.id}"]`);
                if (!deletedAsset) {
                    return;
                }

                // delete row with an animation
                deletedAsset.classList.add('is-deleting');
                window.setTimeout(() => {
                    deletedAsset.remove();
                    this._decreaseCounter(asset.type);
                }, 500);

                this.dialogTarget.classList.remove('is-active');
                this.dialogConfirmTarget.classList.remove('is-loading');
            })
        ;
    }
}
