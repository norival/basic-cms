import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
    };

    static targets = [
        'dialog',
        'preview',
    ];

    confirm(event) {
        const url = this.dialogTarget.dataset.url;

        fetch(url, {
            method: 'DELETE',
        })
            .then(response => response.json())
            .then(id => {
                const deletedRow = this.element.querySelector(`tr[data-contact-id="${id}"]`);
                if (!deletedRow) {
                    return;
                }

                // delete row with an animation
                deletedRow.classList.add('is-deleting');
                window.setTimeout(() => {
                    deletedRow.remove();
                }, 500);

                this.dialogTarget.classList.remove('is-active');
                this.previewTarget.innerHTML = '';
            })
        ;
    }

    delete(event) {
        event.preventDefault();
        const url = event.currentTarget.dataset.url;

        if (url) {
            this.dialogTarget.dataset.url = url;
            this.dialogTarget.classList.add('is-active');
        }
    }

    show(event) {
        event.preventDefault();
        const url = event.currentTarget.dataset.url;

        if (url) {
            fetch(url)
                .then(response => response.text())
                .then(text => {
                    this.previewTarget.innerHTML = text;
                })
            ;
        }
    }
}
