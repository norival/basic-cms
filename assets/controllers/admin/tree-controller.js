import { Controller } from 'stimulus';
const slug = require('slug')

export default class extends Controller {
    static values = {
        addNodeUrl: String,
        parentId: String,
    };

    static targets = [
        'title',
    ];

    connect() {
        this.hasNewNodeOpen = false;
    }

    addNode(event) {
        if (this.hasNewNodeOpen) {
            return;
        }

        this.hasNewNodeOpen = true;
        this.element.insertAdjacentHTML(
            'beforeend',
            `
            <span
                class="new-node-container field has-addons">
                <span class="control">
                    <input
                        class="input is-small"
                        type="text"
                        placeholder="Nom du noeud"
                        data-admin--tree-target="title"
                        data-action="keydown->admin--tree#confirmNewNode">
                </span>
                <span class="control">
                    <button
                        class="button is-small is-success"
                        data-action="admin--tree#confirmNewNode">
                        <span class='icon'>
                            <i class='fas fa-check'></i>
                        </span>
                    </button>
                </span>
                <span class="control">
                    <button
                        class="button is-small is-danger"
                        data-action="admin--tree#cancelNewNode">
                        <span class='icon'>
                            <i class='fas fa-times'></i>
                        </span>
                    </button>
                </span>
            </span>
            `
        );

        this.element.querySelector('.new-node-container input').focus();
    }

    cancelNewNode() {
        this.element.querySelector('.new-node-container')?.remove();
        this.hasNewNodeOpen = false;
    }

    confirmNewNode(event) {
        if (event.key && event.key !== 'Enter') {
            return;
        }

        fetch(this.addNodeUrlValue, {
            method: 'POST',
            body: JSON.stringify({
                title: this.titleTarget.value,
                slug: slug(this.titleTarget.value),
            }),
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then(data => {
                window.location.href = data.path;
            })
            .catch(error => {
                console.error('Cannot add node');
            });
    }
}
