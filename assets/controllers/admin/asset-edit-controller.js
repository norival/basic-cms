import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
        url: String,
    };

    static targets = [
        'fileInput',
        'replaceModal',
    ];

    openModal(event) {
        event.preventDefault();
        this.replaceModalTarget.classList.add('is-active');
    }

    replace() {
        if (this.fileInputTarget.files === null) {
            return;
        }

        const data = new FormData();
        const file = this.fileInputTarget.files[0];

        if (!file) {
            return;
        }

        data.append('media', file, file.name)

        fetch(this.urlValue, {
            method: 'POST',
            body: data,
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }

                throw new Error(response.statusText);
            })
            .then(data => {
                window.location.href = data.path;
            })
            .catch(error => console.log(error.message));
    }
}
