import '../../js/editor/image.scss';
import '../../js/editor/image_grid.scss';
import '../../js/editor/image_wrap.scss';

import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header'; 
import { Image } from '../../js/editor/Image';
import ImageGrid from '../../js/editor/ImageGrid';
import ImageWrap from '../../js/editor/ImageWrap';
import List from '@editorjs/list';
import LinkAutocomplete from '@editorjs/link-autocomplete';

import Cropper from "cropperjs";
import 'cropperjs/dist/cropper.css';

import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
        addHeaderUrl: String,
        addToPageUrl: String,
        headerAssetId: String,
        headerId: String,
        cropEditUrl: String,
        cropNewUrl: String,
        csrfToken: String,
        isEdit: Boolean,
        removeFromPageUrl: String,
        removeHeaderUrl: String,
        searchUrl: String,
        uploadUrl: String,
    };

    static targets = [
        'changeHeaderButton',
        'chooseHeaderButton',
        'confirmCropButton',
        'headerCropper',
        'displayTitle',
        'imageCollection',
        'imageCollectionCloseButton',
        'pageHeader',
        'startCropButton',
        'state',
        'stateText',
        'stateTextError',
        'title',
        // 'titleInput',
        'titlePlaceholder',
    ];

    editor;
    input;

    connect() {
        this.input = document.getElementById('page_content');
        this.isSynced = true;
        this.isSyncing = false;

        const editorJSConfig = {};
        this.editor = new EditorJS({
            holder: 'editorjsHolder',
            logLevel: 'ERROR',
            data: JSON.parse(this.input.value),
            onChange: this._onEditorChange,
            placeholder: 'Écrivez ici',
            tools: {
                header: {
                    class: Header,
                    config: {
                        levels: [2, 3],
                        defaultLevel: 2,
                        placeholder: 'Insérez un titre',
                    },
                },
                list: List,
                link: {
                    class: LinkAutocomplete,
                    config: {
                        endpoint: this.searchUrlValue,
                        queryParam: 'search'
                    }
                },
                image: {
                    class: Image,
                    config: {
                        baseUrl: '/uploads/assets/',
                        cropEditUrl: '/api/asset/crop/ID/CROPPED_ID/edit',
                        cropNewUrl: '/api/asset/crop/ID',
                        fetchUrl: '/api/asset/image',
                        uploadUrl: this.uploadUrlValue,
                        addToPageUrl: this.addToPageUrlValue,
                        removeFromPageUrl: this.removeFromPageUrlValue,
                    }
                },
                imageWrap: {
                    class: ImageWrap,
                    config: {
                        uploadUrl: this.uploadUrlValue,
                        addToPageUrl: this.addToPageUrlValue,
                        removeFromPageUrl: this.removeFromPageUrlValue,
                    },
                },
                imageGrid: {
                    class: ImageGrid,
                    config: {
                        imagesPerLine: 2,
                        uploadUrl: this.uploadUrlValue,
                    },
                }
            },
        });

        // prevent closing of tab/window
        window.addEventListener('beforeunload', this._onBeforeUnload);

        // disable auto save if new page
        if (this.isEditValue) {
            // trigger editor change when changing configuration or title
            this.element.addEventListener('change', () => this._onEditorChange());
            // this.titleInputTarget?.addEventListener('input', () => this._onEditorChange());

            // this.element.addEventListener('submit', (event) => event.preventDefault());

            // set sync interval
            setInterval(this._sync, 3000);
        }
    }

    disconnect() {
        window.removeEventListener('beforeunload', this._onBeforeUnload);
    }

    _onBeforeUnload = (event) => {
        if (!this.isSynced) {
            // event.preventDefault();
            event.returnValue = '';
        }
    }

    _onEditorChange = () => {
        this.isSynced = false;
        this.editor.save().then(data => {
            this.input.value = JSON.stringify(data);
        });
    }

    _onFormSubmit = (event) => {
        event.preventDefault();
    }

    _sync = () => {
        if (this.isSyncing || this.isSynced) {
            return;
        }

        this.isSyncing = true;

        const data = new FormData(this.element);
        const url = this.element.action;

        fetch(url, {
            method: 'POST',
            body: data,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
            },
        })
            .then(response => {
                this.isSyncing = false;
                this.isSynced = response.ok;
            })
        ;
    }

    chooseImage(event) {
        if (!event.target.classList.contains('image')) {
            return;
        }

        // handle double-click
        if (this.currentClickingImage === event.target.dataset.assetId) {
            this.chooseHeaderButtonTarget.click();
            return;
        }

        this.imageCollectionTarget.querySelectorAll('.image.is-selected')?.forEach(element => {
            element.classList.remove('is-selected');
        });

        // save clicking state for double click
        this.currentClickingImage = event.target.dataset.assetId;
        setTimeout(() => this.currentClickingImage = null, 500)

        event.target.classList.add('is-selected');
        this.chooseHeaderButtonTarget.removeAttribute('disabled');
    }

    resetImageLibrary() {
        this.imageCollectionTarget.querySelectorAll('.image.is-selected')?.forEach(element => {
            element.classList.remove('is-selected');
        });

        this.chooseHeaderButtonTarget.setAttribute('disabled', 'disabled');
        this.imageCollectionTarget.querySelector('.image-collection-container').scrollTo(0, 0);
    }

    onClickChooseHeader() {
        const selectedImage = this.imageCollectionTarget.querySelector('.image.is-selected');

        if (selectedImage.dataset.assetId && selectedImage.dataset.assetPath) {
            // this.headerIdTarget.value = selectedImage.dataset.assetId;
            this.isSynced = false;

            // TODO: error handling
            const url = this.addHeaderUrlValue.replace('ASSET_ID', selectedImage.dataset.assetId);
            fetch(url, {
                method: 'POST',
                body: JSON.stringify({
                    csrfToken: this.csrfTokenValue,
                }),
            });

            this.pageHeaderTarget.style.backgroundImage = `url(${selectedImage.dataset.assetPath})`;
            this.headerAssetIdValue = selectedImage.dataset.assetId;
            this.startCropButtonTarget.classList.remove('hidden');
            this.headerCropperTarget.src = selectedImage.dataset.assetPath;
        }

        this.imageCollectionCloseButtonTarget.click();
    }

    onChooseNewImage(event) {
        if (!(event.target.files && event.target.files.length > 0)) {
            throw new Error('File input is empty');
        }

        const file = event.target.files[0];
        const data = new FormData();

        data.append('media', file, file.name);

        fetch(this.uploadUrlValue, {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then((data) => {
                this
                    .imageCollectionTarget
                    .querySelector('.image-collection')
                    .insertAdjacentHTML('afterbegin', data.renderedAsset);
                this.resetImageLibrary();

                this
                    .imageCollectionTarget
                    .querySelector(`.image[data-asset-id="${data.asset.id}"]`)
                    .classList.add('is-selected');
                this.chooseHeaderButtonTarget.removeAttribute('disabled');
                this.chooseHeaderButtonTarget.dispatchEvent(new Event('click'));
            });
    }

    removeHeader() {
        fetch(this.removeHeaderUrlValue, {
            method: 'POST',
            body: JSON.stringify({
                csrfToken: this.csrfTokenValue,
            }),
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                }

                throw new Error('Impossible to remove header');
            })
            .then(() => {
                this.pageHeaderTarget.style.backgroundImage = '';
            })
            .catch(error => {
                console.error('Cannot remove header');
            });
    }

    cropHeader = () => {
        this.headerCropperTarget.classList.remove('hidden');
        this.headerCropper = new Cropper(this.headerCropperTarget, {
            autoCrop: true,
            autoCropArea: 1,
            cropBoxResizable: false,
            cropBoxMovable: false,
            dragMode: 'move',
            viewMode: 3,
        });

        this.changeHeaderButtonTarget.classList.add('hidden');
        this.startCropButtonTarget.classList.add('hidden');
        this.confirmCropButtonTarget.classList.remove('hidden');
    }

    confirmCrop = () => {
        const cropData = this.headerCropper.getData();

        this.startCropButtonTarget.classList.remove('hidden');
        this.confirmCropButtonTarget.classList.add('hidden');
        this.headerCropperTarget.classList.add('hidden');

        const url = this.headerIdValue
            ? this.cropEditUrlValue.replace('CROPPED_ID', this.headerIdValue).replace('ASSET_ID', this.headerAssetIdValue)
            : this.cropNewUrlValue.replace('ASSET_ID', this.headerAssetIdValue);

        fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                cropData: cropData,
            }),
        })
            .then(response => response.json())
            .then(data => {
                this.headerIdValue = data.id;
                this.pageHeaderTarget.style.backgroundImage = `url(${data.publicPath})`;
                this.changeHeaderButtonTarget.classList.remove('hidden');
            });

        this.headerCropper.destroy();
    }
}
