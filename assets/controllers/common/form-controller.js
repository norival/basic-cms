import { Controller } from 'stimulus';

export default class extends Controller {
    connect() {
        this.submitButtons = this.element.querySelectorAll('[type=submit]');
    }

    load(event) {
        event.target.classList.add('is-loading');
        this.submitButtons.forEach(button => button.classList.add('is-loading'));
    }
}
