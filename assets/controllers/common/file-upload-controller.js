import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
    };

    static targets = [
        'fileName',
    ];

    connect() {
        console.log('coucou');
    }

    setName(event) {
        const input = event.target;

        this.fileNameTarget.innerHTML = input.files[0].name;
    }
}
