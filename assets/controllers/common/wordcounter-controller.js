import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
    };

    static targets = [
        'currentCount',
        'input',
        'maxCount',
    ];

    connect() {
        this.maxCount = this.inputTarget.getAttribute('maxLength');
        this.maxCountTarget.textContent = this.maxCount;

        this.update();
    }

    update = () => {
        this.currentCount = this.inputTarget.value.length ?? 0;
        this.currentCountTarget.textContent = this.currentCount;
    }
}
