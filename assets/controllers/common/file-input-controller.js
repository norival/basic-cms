import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = [
        'input',
        'preview',
        'previewContainer',
    ];

    connect() {
        this.inputTarget.addEventListener('change', this._onChangeInput);

        if (this.previewTarget.src !== '') {
            this.previewContainerTarget.classList.remove('hidden');
        }
    }

    _onChangeInput = () => {
        const file = this.inputTarget.files[0];
        if (file) {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.addEventListener('load', () => {
                this.previewTarget.src = fileReader.result;
                this.previewContainerTarget.classList.remove('hidden');
            });
        }
    }
}
