import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
        fetchUrl: String,
        isViewport: Boolean,
        isEmbed: Boolean,
        offset: Number,
        perPage: Number,
    };

    static targets = [
        'collection',
        'loader',
    ];

    connect() {
        this.observer = new IntersectionObserver(
            this._load,
            {
                root: this.isViewportValue ? null : this.element,
                rootMargin: '0px',
                threshold: 0.1,
            }
        )

        this.observer.observe(this.loaderTarget);
    }

    /**
     * Handle lazy loading for discussions
     *
     * @param {IntersectionObserverEntry[]} entries
     * @param {IntersectionObserver} observer
     */
    _load = (entries, observer) => {
        entries.forEach(entry => {
            if (
                !entry.target.classList.contains('app-loader')
                || !entry.isIntersecting
                || this.isLoading
            ) {
                return;
            }

            const target = entry.target;

            // save loading state in the element dataset
            this.isLoading = true;

            // query string parameters
            const params = {
                isEmbed: this.isEmbedValue,
                offset: this.offsetValue >= 0 ? this.offsetValue + this.perPageValue : 0,
                perPage: this.perPageValue,
            };

            // build URL with current parameters (eg https://baseurl?offset=30&perPage=30)
            const url = new URL(this.fetchUrlValue);
            url.search = new URLSearchParams(params).toString();

            fetch(url)
                .then(response => response.json())
                .then(data => {
                    // store new offset
                    this.offsetValue = params.offset;
                    this.isLoading = false;

                    this.collectionTarget.insertAdjacentHTML('beforeend', data.content);

                    if (data.isLastPage) {
                        this.loaderTarget.remove();
                        observer.disconnect();
                    }
                });
        });
    }
}
