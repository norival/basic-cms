
import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
    };

    static targets = [
        'content',
        'tabs',
    ];

    changeTab(event) {
        event.preventDefault();

        this.tabsTarget.querySelectorAll('li').forEach(el => {
            el.classList.remove('is-active');
        });
        event.currentTarget.classList.add('is-active');

        this.contentTarget.querySelectorAll('[data-target]').forEach(el => {
            el.classList.remove('is-active');

            if (el.dataset.target === event.currentTarget.dataset.target) {
                el.classList.add('is-active');
            }
        })
    }
}
