import { Controller } from 'stimulus';
import Sortable from 'sortablejs';

export default class extends Controller {
    static targets = [
    ];

    connect() {
        this.sortbale = new Sortable(this.element, {
            group: 'nested',
            animation: 150,
            fallbackOnBody: true,
            swapThreshold: 0.65,
            onEnd: this._onEnd,
            filter: '.static',
        });
    }

    _onEnd = (event) => {
        const item = event.item;
        const parent = item.closest(`[data-tree-node-id]:not([data-tree-node-id="${item.dataset.treeNodeId}"])`);

        const items = item.closest('.tree-container')?.querySelectorAll('li');

        const order = {};
        for (let i = 0; i < items.length; i++) {
            order[items[i].dataset.treeNodeId] = i;
        }

        fetch(item.dataset.sortUrl, {
            method: 'POST',
            body: JSON.stringify({
                parent: parent?.dataset.treeNodeId ?? '',
                order: order,
            }),
        })
            .then(response => response.json())
            .then(data => console.log(data));
    }
}
