import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
        open: Boolean,
    };

    static targets = [
        'content',
        'header',
        'toggle',
    ];

    connect() {
        if (this.openValue) {
            this.open();
        }
    }

    toggle(event) {
        // do not toggle if the target is a button
        if (event.target.closest('button:not(.card-header-icon)')) {
            return;
        }

        event.preventDefault();

        if (this.openValue) {
            this.close();
            return;
        }

        this.open();
    }

    close() {
        this.toggleTarget.classList.remove('fa-angle-up');
        this.toggleTarget.classList.add('fa-angle-down');
        this.contentTarget.classList.add('is-hidden');
        this.openValue = false;
    }

    open() {
        this.toggleTarget.classList.remove('fa-angle-down');
        this.toggleTarget.classList.add('fa-angle-up');
        this.contentTarget.classList.remove('is-hidden');
        this.openValue = true;
    }
}
