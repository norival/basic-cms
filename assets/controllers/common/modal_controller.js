import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
    };

    static targets = [
        'modal',
    ];

    /**
     * open.
     *
     * @param {Event} event
     */
    open(event) {
        event.preventDefault();
        this.modalTarget?.classList.add('is-active');
        document.querySelector('html')?.classList.add('is-clipped');
    }

    close() {
        this.modalTarget?.classList.remove('is-active');
        document.querySelector('html')?.classList.remove('is-clipped');
    }
}
