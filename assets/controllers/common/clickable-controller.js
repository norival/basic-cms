import { Controller } from 'stimulus';

export default class extends Controller {
    static values = {
    };

    static targets = [
        'mainLink',
    ];

    connect() {
        this.element.querySelectorAll('a, button')?.forEach(element => {
            element.addEventListener('click', e => e.stopPropagation());
        });
    }

    click() {
        const isTextSelected = window.getSelection().toString();
        if (!isTextSelected) {
            Turbo.visit(this.mainLinkTarget.href);
        }
    }
}
