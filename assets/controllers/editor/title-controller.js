import { Controller } from 'stimulus';
import { v4 as uuidv4 } from 'uuid';

export default class extends Controller {
    static targets = [
        'input',
        'placeholder',
        'slug',
        'title',
    ];

    static values = {
        max: Number,
    };

    updateTitle = () => {
        const title = this.titleTarget.innerHTML.trim().replace("&nbsp;", ""); 
        this.inputTarget.value = title;

        document.dispatchEvent(
            new CustomEvent(
                'title:update',
                {
                    detail: {
                        title: title,
                    },
                }
            )
        );

        if (title === '') {
            this.placeholderTarget.classList.remove('hidden');
            this.inputTarget.value = "Titre de la page";
        } else {
            this.placeholderTarget.classList.add('hidden');
        }
    }

    preventLineBreak(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
        }
    }

    preventOverflow(event) {
        if (!this.hasMaxValue || event.key === 'Backspace') {
            return;
        }

        if (event.target.innerHTML.trim().length > this.maxValue) {
            event.preventDefault();
        }
    }
}
