import './styles/admin/index.scss';

// start the Stimulus application
import './bootstrap';

import './turbo/turbo-helper';

require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');
