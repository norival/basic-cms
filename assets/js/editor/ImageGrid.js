import * as utils from '../utils';

export default class ImageGrid {
    constructor({config, data, api}) {
        this.config = config;
        this.data = data;
        this.api = api;

        if (!this.data.images) {
            this.data.images = [];
        }

        if (!this.data.imagesPerLine) {
            this.data.imagesPerLine = this.config.imagesPerLine;
        }

        this.colWidth = 12 / this.data.imagesPerLine;
        this.element = undefined;
    }

    render() {
        const collectionModal = document.querySelector('#tplEditorImageCollection').content.cloneNode(true);

        this.element = document.createElement('div');
        this.element.innerHTML = `
            <div class="image-grid-settings">
                <div class="field is-horizontal">
                    <div class="field-label is-small">
                        <label class="label">Nombre par ligne</label>
                    </div>
                    <div class="field-body">
                        <div class="field is-narrow">
                            <div class="control">
                                <input
                                    class="input is-small image-grid-perline"
                                    type="number"
                                    min="1"
                                    max="6"
                                    placeholder="Nombre d'images par ligne"
                                    value="${this.data.imagesPerLine}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns is-multiline image-grid is-centered is-1">
                ${this._renderImages()}
                <div class="column add-button is-${this.colWidth}">
                    <button>Ajouter une image</button>
                </div>
            </div>
        `;

        this.element.appendChild(collectionModal);

        this.collectionModal = this.element.querySelector('.image-collection-modal');
        this.confirmButton = this
            .collectionModal
            .querySelector("[data-button-action='image-collection-confirm-button']");

        this.element.addEventListener('change', this._updateCaption);
        this.element.addEventListener('click', this._handleAction);
        this.element.querySelector('.add-button')?.addEventListener('click', this._onClickAddImage);
        this.element.querySelector('.image-grid-perline')?.addEventListener('change', this._changeImagesPerLine);
        this.collectionModal.querySelector('.image-collection').addEventListener('click', this._selectImage);
        this.collectionModal.querySelector('.file-input').addEventListener('change', this._uploadImage);
        this.confirmButton.addEventListener('click', this._chooseImage);

        return this.element;
    }

    static get toolbox() {
        return {
            title: 'ImageGrid',
            icon: '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="th-large" class="svg-inline--fa fa-th-large fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M296 32h192c13.255 0 24 10.745 24 24v160c0 13.255-10.745 24-24 24H296c-13.255 0-24-10.745-24-24V56c0-13.255 10.745-24 24-24zm-80 0H24C10.745 32 0 42.745 0 56v160c0 13.255 10.745 24 24 24h192c13.255 0 24-10.745 24-24V56c0-13.255-10.745-24-24-24zM0 296v160c0 13.255 10.745 24 24 24h192c13.255 0 24-10.745 24-24V296c0-13.255-10.745-24-24-24H24c-13.255 0-24 10.745-24 24zm296 184h192c13.255 0 24-10.745 24-24V296c0-13.255-10.745-24-24-24H296c-13.255 0-24 10.745-24 24v160c0 13.255 10.745 24 24 24z"></path></svg>',
        };
    }

    save(blockContent) {
        return {
            imagesPerLine: blockContent.querySelector('.image-grid-perline').value,
            images: this.data.images,
        };
    }

    validate(savedData){
        if (savedData.images.length === 0) {
            return false;
        }

        return true;
    }

    /**
     * @param {Event} event
     */
    _changeImagesPerLine = (event) => {
        const oldColWidth = this.colWidth;

        this.data.imagesPerLine = event.target.value;
        this.colWidth = 12 / this.data.imagesPerLine;

        this.element.querySelectorAll('.image-grid .column')?.forEach(column => {
            column.classList.remove(`is-${oldColWidth}`);
            column.classList.add(`is-${this.colWidth}`);
        });
    }

    /**
     * @param {MouseEvent} event
     */
    _onClickAddImage = (event) => {
        this.collectionModal.classList.add('is-active');
    }

    _renderImages() {
        return this.data.images.reduce(
            (html, image) => `${html}${this._renderImage(image)}`,
            ''
        );
    }

    /**
     * @param {{id: string, gridId: string, path: string, caption: string}} image
     */
    _renderImage = (image) => {
        return `
            <div class="column is-${this.colWidth}">
                <div class="image-grid-image-container">
                    <img
                        class="image-grid-image"
                        src="${image.path}"
                        data-asset-id="${image.id}"
                        data-grid-id="${image.gridId}"
                        alt="">
                    <textarea
                        class="image-grid-image-caption"
                        data-grid-id="${image.gridId}"
                        id=""
                        rows="5">${image.caption ?? ''}</textarea>
                    <button class="image-grid-delete">
                        <span class="icon">
                            <i class="fas fa-trash"></i>
                        </span>
                    </button>
                </div>
            </div>
        `;
    }

    /**
     * @param {MouseEvent} event
     */
    _selectImage = (event) => {
        if (!event.target.classList.contains('image')) {
            return;
        }

        // handle double-click
        if (this.currentClickingImage === event.target.dataset.assetId) {
            this.confirmButton.click();
            return;
        }

        this
            .collectionModal
            .querySelectorAll('.image-collection-container .image.is-selected')
            ?.forEach(element => {
                element.classList.remove('is-selected');
            });

        // save clicking state for double click
        this.currentClickingImage = event.target.dataset.assetId;
        setTimeout(() => this.currentClickingImage = null, 500)

        event.target.classList.add('is-selected');
        this.confirmButton.removeAttribute('disabled');
    }

    /**
     * @param {MouseEvent} event
     */
    _chooseImage = (event) => {
        const selectedImage = this.collectionModal.querySelector('.image.is-selected');

        if (selectedImage.dataset.assetId && selectedImage.dataset.assetPath) {
            const gridId = utils.randomString();

            this.data.images.push({
                id: selectedImage.dataset.assetId,
                path: selectedImage.dataset.assetPath,
                caption: '',
                gridId: gridId,
            });

            this.element.querySelector('.add-button').insertAdjacentHTML(
                'beforebegin',
                this._renderImage({
                    id: selectedImage.dataset.assetId,
                    path: selectedImage.dataset.assetPath,
                    gridId: gridId,
                })
            );
        }

        this.collectionModal.querySelector('.modal-background').click();
    }

    _uploadImage = (event) => {
        if (!(event.target.files && event.target.files.length > 0)) {
            throw new Error('File input is empty');
        }

        const file = event.target.files[0];
        const data = new FormData();

        data.append('media', file, file.name);

        fetch(this.config.uploadUrl, {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then((data) => {
                this
                    .collectionModal
                    .querySelector('.image-collection')
                    .insertAdjacentHTML('afterbegin', data.renderedAsset);

                this
                    .collectionModal
                    .querySelector(`.image[data-asset-id="${data.asset.id}"]`)
                    .click();

                this.confirmButton.dispatchEvent(new Event('click'));
            });
    }

    _updateCaption = (event) => {
        if (!event.target.classList.contains('image-grid-image-caption')) {
            return;
        }

        const image = this.data.images.find(image => image.gridId === event.target.dataset.gridId);

        if (image) {
            image.caption = event.target.value;
        }
    }

    _handleAction = (event) => {
        const actionButton = event.target.closest('.image-grid-delete');
        if (!actionButton) {
            return;
        }

        const container = actionButton.closest('.column');
        const img = container.querySelector('.image-grid-image');
        this.data.images = this.data.images.filter(image => image.gridId !== img.dataset.gridId);
        container.remove();
    }
}
