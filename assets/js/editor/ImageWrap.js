export default class ImageWrap {
    constructor({config, data, api}) {
        this.config = config;
        this.data = data;
        this.api = api;

        this.element = undefined;
    }

    render() {
        const collectionModal = document.querySelector('#tplEditorImageCollection').content.cloneNode(true);

        this.element = document.createElement('div');
        this.element.innerHTML = `
            <div class="image-wrap-settings">
                <div class="field is-horizontal">
                    <div class="field-label is-small">
                        <label class="label">Mise en page :</label>
                    </div>
                    <div class="field-body">
                        <div class="field is-narrow">
                            <div class="control">
                                <div class="select is-small">
                                    <select class="change-layout">
                                        ${this._renderOptions()}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns image-wrap-container block">
                <div
                    class="column is-4 image-wrap-image"
                    data-asset-id="${this.data?.image?.id}"
                    data-asset-path="${this.data?.image?.path}">
                    <button class="button is-small is-primary add-image-button">
                        Choisir une image
                    </button>
                    <img src="${this.data?.image?.path}">
                </div>
                <div class="column is-8 image-wrap-text">
                    <textarea class="image-wrap-text-input">${this.data?.text?.content ?? ''}</textarea>
                </div>
            </div>
        `;

        this.element.appendChild(collectionModal);
        this.collectionModal = this.element.querySelector('.image-collection-modal');
        this.confirmButton = this
            .collectionModal
            .querySelector("[data-button-action='image-collection-confirm-button']");
        this.input = this.element.querySelector('.image-wrap-text-input');
        this.input.addEventListener('keydown', this._onKeypressInput);

        this.collectionModal.querySelector('.image-collection').addEventListener('click', this._selectImage);
        this.collectionModal.querySelector('.file-input').addEventListener('change', this._uploadImage);
        this.confirmButton.addEventListener('click', this._chooseImage);

        this.element.querySelector('.change-layout').addEventListener('change', this._onChangeLayout);
        this.element.querySelector('.add-image-button')?.addEventListener('click', this._onClickAddImage);

        this.element.querySelector('.change-layout').dispatchEvent(new Event('change'));

        return this.element;
    }

    /**
     * Stop propagation of key if enter, this allow creating new lines
     *
     * @param {KeyboardEvent} event
     */
    _onKeypressInput = (event) => {
        if (event.key === 'Backspace' || (event.key === 'Enter' && !event.ctrlKey)) {
            event.stopPropagation();
        }
    }

    static get toolbox() {
        return {
            title: 'ImageWrap',
            icon: '<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="grip-horizontal" class="svg-inline--fa fa-grip-horizontal fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M96 288H32c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zM96 96H32c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32zm160 0h-64c-17.67 0-32 14.33-32 32v64c0 17.67 14.33 32 32 32h64c17.67 0 32-14.33 32-32v-64c0-17.67-14.33-32-32-32z"></path></svg>',
        };
    }

    save(blockContent) {
        return {
            image: {
                id: blockContent.querySelector('.image-wrap-image').dataset.assetId,
                path: blockContent.querySelector('.image-wrap-image').dataset.assetPath,
            },
            text: {
                content: this.input.value,
            },
            layout: blockContent.querySelector('.change-layout').value,
        };
    }

    _onChangeLayout = (event) => {
        const container = this.element.querySelector('.image-wrap-container');
        const image = this.element.querySelector('.image-wrap-image');
        const text = this.element.querySelector('.image-wrap-text');

        image.classList.remove('is-4', 'is-6', 'is-8');
        text.classList.remove('is-4', 'is-6', 'is-8');

        switch (event.target.value) {
            case '0':
                container.prepend(image);
                image.classList.add('is-4');
                text.classList.add('is-8');
                break;
            case '1':
                container.prepend(image);
                image.classList.add('is-6');
                text.classList.add('is-6');
                break;
            case '2':
                container.prepend(image);
                image.classList.add('is-8');
                text.classList.add('is-4');
                break;
            case '3':
                container.prepend(text);
                image.classList.add('is-8');
                text.classList.add('is-4');
                break;
            case '4':
                container.prepend(text);
                image.classList.add('is-6');
                text.classList.add('is-6');
                break;
            case '5':
                container.prepend(text);
                image.classList.add('is-4');
                text.classList.add('is-8');
                break;
        }
    }

    _onClickAddImage = () => {
        this.collectionModal.classList.add('is-active');
    }

    /**
     * @param {MouseEvent} event
     */
    _selectImage = (event) => {
        if (!event.target.classList.contains('image')) {
            return;
        }

        // handle double-click
        if (this.currentClickingImage === event.target.dataset.assetId) {
            this.confirmButton.click();
            return;
        }

        this
            .collectionModal
            .querySelectorAll('.image-collection-container .image.is-selected')
            ?.forEach(element => {
                element.classList.remove('is-selected');
            });

        // save clicking state for double click
        this.currentClickingImage = event.target.dataset.assetId;
        setTimeout(() => this.currentClickingImage = null, 500)

        event.target.classList.add('is-selected');
        this.confirmButton.removeAttribute('disabled');
    }

    _uploadImage = (event) => {
        if (!(event.target.files && event.target.files.length > 0)) {
            throw new Error('File input is empty');
        }

        const file = event.target.files[0];
        const data = new FormData();

        data.append('media', file, file.name);

        fetch(this.config.uploadUrl, {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then((data) => {
                this
                    .collectionModal
                    .querySelector('.image-collection')
                    .insertAdjacentHTML('afterbegin', data.renderedAsset);

                this
                    .collectionModal
                    .querySelector(`.image[data-asset-id="${data.asset.id}"]`)
                    .click();

                this.confirmButton.dispatchEvent(new Event('click'));
            });
    }

    removed() {
        // remove the corresponding PageAsset entity
        if (this.data?.image?.id) {
            fetch(this.config.removeFromPageUrl.replace(/PAGE_ASSET_ID/i, this.data.image.id));
        }
    }

    /**
     * @param {MouseEvent} event
     */
    _chooseImage = (event) => {
        const selectedImage = this.collectionModal.querySelector('.image.is-selected');
        const image = this.element.querySelector('.image-wrap-image');

        // remove old image from page
        if (this.data?.image?.id) {
            fetch(this.config.removeFromPageUrl.replace(/PAGE_ASSET_ID/i, this.data.image.id));
        }

        if (selectedImage.dataset.assetId && selectedImage.dataset.assetPath) {
            image.querySelector('img').src = selectedImage.dataset.assetPath;
            image.dataset.assetPath = selectedImage.dataset.assetPath;

            fetch(this.config.addToPageUrl.replace(/ASSET_ID/i, selectedImage.dataset.assetId ?? ''))
                .then(response => response.json())
                .then(id => {
                    image.dataset.assetId = id;

                    if (!this.data.image) {
                        this.data.image = {};
                    }

                    this.data.image.id = id;
                });
        }

        this.collectionModal.querySelector('.modal-background').click();
    }

    _renderOptions() {
        const layouts = [
            {value: 0, label: 'Image : 30% - Texte : 70%'},
            {value: 1, label: 'Image : 50% - Texte : 50%'},
            {value: 2, label: 'Image : 70% - Texte : 30%'},
            {value: 3, label: 'Texte : 30% - Image : 70%'},
            {value: 4, label: 'Texte : 50% - Image : 50%'},
            {value: 5, label: 'Texte : 70% - Image : 30%'},
        ];

        return layouts.reduce((accu, layout) => {
            let html = `<option value="${layout.value}"`;
            if (
                (layout.value == 0 && this.data.layout === undefined)
                || layout.value == this.data.layout
            ) {
                html = `${html} selected`;
            }

            return `${accu}${html}>${layout.label}</option>`;
        }, '')
    }
}
