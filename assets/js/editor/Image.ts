import Cropper from "cropperjs";
import 'cropperjs/dist/cropper.css';

interface IConfig {
    baseUrl: string;
    cropEditUrl: string;
    cropNewUrl: string;
    fetchUrl: string;
    uploadUrl: string;
    addToPageUrl: string;
    removeFromPageUrl: string;
}

interface IData {
    caption?: string;
    id?: string;
    pageAssetId: string;
    path?: string;
    crop: {
        id?: string;
        rect?: Cropper.Data;
        path?: string;
    };
}

interface IInit {
    config: IConfig;
    data: IData;
}

interface IToolbox {
    title: string;
    icon: string;
}

interface IImage {
    caption: string|null;
    createdAt: string;
    id: string;
    path: string;
    title: string;
    type: number;
}

interface INewImageData {
    asset: IImage;
    pageAssetId: string;
}

interface ICroppedAsset {
    id: string;
    path: string;
    publicPath: string;
}

interface ISetting {
    name: string;
    icon: string;
}

export class Image {
    private addButton: HTMLElement|null = null;
    private caption?: string;
    private collectionModal: HTMLElement|null = null;
    private config: IConfig;
    private confirmButton: HTMLElement|null = null;
    private cropper?: Cropper;
    private currentClickingImage: string|null = '';
    private data: IData;
    private el = document.createElement('div');
    private isCropping = false;
    private isNewFile = false;
    private settings: ISetting[] = [];

    constructor({config, data}: IInit) {
        this.config = config;
        this.data = data;

        if (this.config.baseUrl === undefined) {
            throw Error('Invalid configuration. config.baseUrl must not be null');
        }

        if (this.config.fetchUrl === undefined) {
            throw Error('Invalid configuration. config.fetchUrl must not be null');
        }

        if (this.config.uploadUrl === undefined) {
            throw Error('Invalid configuration. config.uploadUrl must not be null');
        }

        if (this.data.id && this.data.path) {
            this.caption = this.data.caption;
        }

        this.settings = [
            {
                name: 'crop',
                icon: `<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M488 352h-40V109.25l59.31-59.31c6.25-6.25 6.25-16.38 0-22.63L484.69 4.69c-6.25-6.25-16.38-6.25-22.63 0L402.75 64H192v96h114.75L160 306.75V24c0-13.26-10.75-24-24-24H88C74.75 0 64 10.74 64 24v40H24C10.75 64 0 74.74 0 88v48c0 13.25 10.75 24 24 24h40v264c0 13.25 10.75 24 24 24h232v-96H205.25L352 205.25V488c0 13.25 10.75 24 24 24h48c13.25 0 24-10.75 24-24v-40h40c13.25 0 24-10.75 24-24v-48c0-13.26-10.75-24-24-24z"></path></svg>`
            },
        ];
    }

    // method to render icon in the toolbox
    static get toolbox(): IToolbox {
        return {
            title: 'Image',
            icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>'
        };
    }

    /**
     * render
     *
     * Render the block
     */
    render() {
        const path = this.data.crop?.path ?? this.data.path;
        this.el.classList.add('editor-image-wrapper', 'is-edit');
        this.el.innerHTML = `
            <figure class="editor-image">
                <img class="" src="${path}" alt="">
                <textarea
                    id="caption"
                    class="caption mt-3"
                    placeholder="Légende de l'image..."
                    value="${this.caption ?? ''}"></textarea>
            </figure>
        `;

        this.el.querySelector('#caption')?.addEventListener('input', this._handleCaptionChange);

        const collectionModalTpl = document.querySelector('#tplEditorImageCollection') as HTMLTemplateElement;
        const collectionModal = collectionModalTpl.content.cloneNode(true);

        this.addButton = document.createElement('button');
        this.addButton.classList.add('button', 'is-small', 'is-primary');
        this.addButton.innerHTML = `Choisir une image`;

        this.el.appendChild(this.addButton);

        this.el.appendChild(collectionModal);
        this.collectionModal = this.el.querySelector('.image-collection-modal') as HTMLElement;

        if (this.collectionModal) {
            this.confirmButton = this
                .collectionModal
                .querySelector("[data-button-action='image-collection-confirm-button']");
            this.collectionModal.querySelector('.image-collection')?.addEventListener('click', this._selectImage);
            this.collectionModal.querySelector('.file-input')?.addEventListener('change', this._uploadImage);
            this.confirmButton?.addEventListener('click', this._handleChooseImage);
        }

        if (this.addButton) {
            this.addButton.addEventListener('click', () => this.collectionModal?.classList.add('is-active'));
        }

        return this.el;
    }

    save(blockContent: HTMLDivElement): IData {
        return {
            id: this.data.id,
            path: this.data.path,
            caption: this.caption ?? '',
            pageAssetId: this.data.pageAssetId,
            crop: {
                id: this.data.crop?.id,
                rect: this.data.crop?.rect,
                path: this.data.crop?.path,
            }
        };
    }

    validate(savedData: IData) {
        return savedData.id !== null && savedData.path !== null;
    }

    renderSettings() {
        const wrapper = document.createElement('div');

        this.settings.forEach(tune => {
            let button = document.createElement('div');

            button.classList.add('cdx-settings-button');
            button.innerHTML = tune.icon;
            wrapper.appendChild(button);

            button.addEventListener('click', (event) => this._handleClickSetting(event, tune.name));
        });

        return wrapper;
    }

    removed() {
        // remove the corresponding PageAsset entity
        if (this.data.pageAssetId) {
            fetch(this.config.removeFromPageUrl.replace(/PAGE_ASSET_ID/i, this.data.pageAssetId));
        }
    }

    _handleClickSetting = (event: Event, name: string) => {
        if (name === 'crop') {
            this.isCropping = !this.isCropping;

            if (this.isCropping) {
                (event.currentTarget  as HTMLElement).classList.add('cdx-settings-button--active');
                this._startCropper();

                return;
            }

            const saveButton = this.el.querySelector(".image-crop-actions [data-action='save']") as HTMLButtonElement;
            if (saveButton) {
                console.log(saveButton);
                saveButton.click();
            }
            (event.currentTarget  as HTMLElement).classList.remove('cdx-settings-button--active');

            return;
        }
    }

    _startCropper() {
        // create crop action buttons
        const buttons = document.createElement('div');
        buttons.classList.add('block', 'image-crop-actions', 'mt-2');
        buttons.innerHTML = `
            <button class="button is-success" data-action="save">Enregistrer</button>
            <button class="button is-danger" data-action="cancel">Annuler</button>
        `;

        buttons.addEventListener('click', this._handleClickCrop);
        this.el.querySelector('img')?.insertAdjacentElement('afterend', buttons);

        // replace src with full image so we can 'decrop' image if we want
        const image = this.el.querySelector('img') as HTMLImageElement;
        image.src = this.data.path as string;

        // instantiate the cropper
        if (this.data.crop?.rect) {
            this.cropper = new Cropper(image, {
                autoCrop: true,
                viewMode: 2,
                data: this.data.crop.rect,
            });
        } else {
            this.cropper = new Cropper(image, {
                autoCrop: true,
                viewMode: 2,
            });
        }
    }

    _handleClickCrop = (event: MouseEvent) => {
        event.preventDefault();
        const button = (event.target as HTMLElement).closest('button');

        if (button && button.dataset.action === 'save') {
            if (!this.data.crop) {
                this.data.crop = {
                    id: '',
                    rect: this.cropper?.getData(true),
                }
            } else {
                this.data.crop.rect = this.cropper?.getData(true);
            }

            // TODO send data to server
            let url = this.data.crop.id
                ? this.config.cropEditUrl
                : this.config.cropNewUrl;

            if (this.data.id) {
                url = url.replace('ID', this.data.id);
            }

            if (this.data.crop.id) {
                url = url.replace('CROPPED_ID', this.data.crop.id);
            }

            fetch(url, {
                method: 'POST',
                body: JSON.stringify({
                    cropData: this.data.crop.rect,
                }),
            })
                .then(response => response.json())
                .then((data: ICroppedAsset) => {
                    const image = this.el.querySelector('.editor-image img') as HTMLImageElement;
                    if (this.data.crop) {
                        this.data.crop.id = data.id;
                    }

                    this.data.crop.path = data.publicPath;
                    image.src = data.publicPath;
                });
        }

        if (this.data.crop?.path) {
            // reload cropped image
            const image = this.el.querySelector('img') as HTMLImageElement;
            image.src = this.data.crop.path as string;
        }

        this.el.querySelector('.image-crop-actions')?.remove();
        this.cropper?.destroy();
        this.isCropping = false;
    }

    _handleChooseImage = (event: Event) => {
        event.preventDefault();
        this.data.crop = {};

        const selectedImage = this.collectionModal?.querySelector('.image.is-selected') as HTMLElement;
        const image = this.el.querySelector('.editor-image img') as HTMLImageElement;

        // remove old image from page
        if (this.data.pageAssetId) {
            fetch(this.config.removeFromPageUrl.replace(/PAGE_ASSET_ID/i, this.data.pageAssetId));
        }

        if (selectedImage.dataset.assetId && selectedImage.dataset.assetPath) {
            image.src = selectedImage.dataset.assetPath;
            image.dataset.assetPath = selectedImage.dataset.assetPath;

            fetch(this.config.addToPageUrl.replace(/ASSET_ID/i, selectedImage.dataset.assetId ?? ''))
                .then(response => response.json())
                .then(id => {
                    image.dataset.assetId = id;

                    this.data.pageAssetId = id;
                    this.data.id = selectedImage.dataset.assetId;
                    this.data.path = selectedImage.dataset.assetPath;
                });
        }

        this.collectionModal?.querySelector('.modal-background')?.dispatchEvent(new Event('click'));
    }

    _handleCaptionChange = (event: Event) => {
        this.caption = (event.target as HTMLInputElement).value;
    }

    _selectImage = (event: Event) => {
        const target = event.target as HTMLElement;
        if (!target.classList.contains('image')) {
            return;
        }

        // handle double-click
        if (this.currentClickingImage === target.dataset.assetId) {
            this.confirmButton?.click();
            return;
        }

        this
            .collectionModal
            ?.querySelectorAll('.image-collection-container .image.is-selected')
            ?.forEach(element => {
                element.classList.remove('is-selected');
            });

        // save clicking state for double click
        this.currentClickingImage = target.dataset.assetId as string;
        setTimeout(() => this.currentClickingImage = null, 500)

        target.classList.add('is-selected');
        this.confirmButton?.removeAttribute('disabled');
    }

    _uploadImage = (event: Event) => {
        const target = event.target as HTMLInputElement;
        if (!(target.files && target.files.length > 0)) {
            throw new Error('File input is empty');
        }

        const file = target.files[0];
        const data = new FormData();

        data.append('media', file, file.name);

        fetch(this.config.uploadUrl, {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then((data) => {
                this
                    .collectionModal
                    ?.querySelector('.image-collection')
                    ?.insertAdjacentHTML('afterbegin', data.renderedAsset);

                this
                    .collectionModal
                    ?.querySelector(`.image[data-asset-id="${data.asset.id}"]`)
                    ?.dispatchEvent(new Event('click', {bubbles: true}));

                this.confirmButton?.dispatchEvent(new Event('click', {bubbles: true}));
            });
    }
}
