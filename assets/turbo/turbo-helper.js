class TurboHelper {
    constructor() {
        document.addEventListener('turbo:submit-start', (event) => {
            event.detail.formSubmission.submitter.toggleAttribute('disabled', true);
            event.detail.formSubmission.submitter.classList.add('is-loading');
        });
    }
}

export default new TurboHelper();
