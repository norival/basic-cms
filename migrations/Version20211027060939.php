<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211027060939 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page DROP INDEX IDX_140AB6202EF91FD8, ADD UNIQUE INDEX UNIQ_140AB6202EF91FD8 (header_id)');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6202EF91FD8');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6202EF91FD8 FOREIGN KEY (header_id) REFERENCES cropped_asset (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page DROP INDEX UNIQ_140AB6202EF91FD8, ADD INDEX IDX_140AB6202EF91FD8 (header_id)');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6202EF91FD8');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6202EF91FD8 FOREIGN KEY (header_id) REFERENCES asset (id) ON DELETE SET NULL');
    }

    public function preUp(Schema $schema): void
    {
        $connection = $this->connection;

        // remove all header relations to asset
        $connection->executeQuery(
            "UPDATE page SET header_id = NULL"
        );
    }
}
