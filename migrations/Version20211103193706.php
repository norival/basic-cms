<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211103193706 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6202EF91FD8');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6202EF91FD8 FOREIGN KEY (header_id) REFERENCES cropped_asset (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6202EF91FD8');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6202EF91FD8 FOREIGN KEY (header_id) REFERENCES cropped_asset (id)');
    }
}
